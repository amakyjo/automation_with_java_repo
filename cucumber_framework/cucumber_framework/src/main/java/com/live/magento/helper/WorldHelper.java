package com.live.magento.helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.live.magento.browsers.Browsers;
import com.live.magento.pages.AbstractPage;
import com.live.magento.pages.HomePage;
import com.live.magento.pages.MobileProductPage;
import com.live.magento.pages.MyAccountPage;
import com.live.magento.pages.ProductPage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.pages.ShoppingCartPage;
import com.live.magento.pages.WelcomePage;

public class WorldHelper extends Browsers {
	
	private AbstractPage abstractPage;
	private HomePage homePage;
	private MyAccountPage myAccountPage;
	private WelcomePage welcomePage;
	private SearchResultPage searchResultPage;
	private MobileProductPage mobileProductPage;
	private ProductPage productPage;
	private ShoppingCartPage cartPage;
	
		
	public AbstractPage getAbstractPage(){
		if(abstractPage == null){
			abstractPage = PageFactory.initElements(driver, AbstractPage.class);
		}
		return abstractPage;
	}
	
	public HomePage getHomePage(){
		if(homePage == null){
			homePage = PageFactory.initElements(driver, HomePage.class);
		}
		return homePage;
	}
	
	public MyAccountPage getMyAccountPage(){
		if(myAccountPage == null){
			myAccountPage = PageFactory.initElements(driver, MyAccountPage.class);
		}
		return myAccountPage;
	}
	
	public WelcomePage getWelcomePage(){
		if(welcomePage == null){
			welcomePage = PageFactory.initElements(driver, WelcomePage.class);
		}
		return welcomePage;
	}
	
	public SearchResultPage getSearchResultPage(){
		if(searchResultPage == null){
			searchResultPage = PageFactory.initElements(driver, SearchResultPage.class);
		}
		return searchResultPage;
	}
	
	public MobileProductPage getMobileProductPage(){
		if(mobileProductPage == null){
			mobileProductPage = PageFactory.initElements(driver, MobileProductPage.class);
		}
		return mobileProductPage;
	}
	
	public ProductPage getProductPage(){
		if(productPage == null){
			productPage = PageFactory.initElements(driver, ProductPage.class);
		}
		return productPage;
	}
	
	public ShoppingCartPage getcartPage(){
		if(cartPage == null){
			cartPage = PageFactory.initElements(driver, ShoppingCartPage.class);
		}
		return cartPage;
	}
	

}
