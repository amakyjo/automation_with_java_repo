package com.live.magento.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TvProductPage extends AbstractPage {

	
	@FindBy(id="product-collection-image-4")
	private WebElement tvLgLcd;
	
	@FindBy(id="product-collection-image-5")
	private WebElement tvSamsung;
	

	public TvProductPage(WebDriver driver) {
		super(driver);
		
	}

	public ProductPage selectTvProductType(String productType){
		try{
			logger.info("The selectTvProductType method has been initiated");	
			if(productType.equalsIgnoreCase("LG LCD")){
				tvLgLcd.click();
			}
			else if(productType.equalsIgnoreCase("Samsung LCD")){
				tvSamsung.click();
			}
			logger.info("tv product has been selected successfully");		
		}
		catch(Throwable t){
			logger.error("The selectTvProductType method has encountered error" +t);
			camera.takeShot("selectTvProductType");
		}
		return PageFactory.initElements(driver, ProductPage.class);
}
}