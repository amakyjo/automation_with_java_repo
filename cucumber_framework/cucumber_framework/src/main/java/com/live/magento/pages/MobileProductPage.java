package com.live.magento.pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MobileProductPage extends AbstractPage{
	

	@FindBy(id="product-collection-image-3")
	private WebElement mobileSamsung;
	
	@FindBy(id="product-collection-image-1")
	private WebElement mobileSony;
	
	@FindBy(id="product-collection-image-2")
	private WebElement mobileIphone;
	

	public MobileProductPage(WebDriver driver) {
		super(driver);
	}

	public ProductPage selectMobileProductType(String productType){
		try{
			logger.info("The selectMobileProductType method has been initiated");	
			if(productType.equalsIgnoreCase("Samsung Galaxy")){
				mobileSamsung.click();
			}
			else if(productType.equalsIgnoreCase("Sony Xperia")){
				mobileSony.click();
			}
			else if(productType.equalsIgnoreCase("IPhone")){
				mobileIphone.click();
			}
				logger.info("mobile product has been selected successfully");		
		}
		catch(Throwable t){
			logger.error("The selectMobileProductType method has encountered error" +t);
			camera.takeShot("selectMobileProductType");
		}
		return PageFactory.initElements(driver, ProductPage.class);
	}
	
	public void verifyPageAndOpenProductPage(){
		try{
			logger.info("The verifyPageAndOpenProductPage method has been initiated");
			boolean result = driver.getPageSource().contains("This is root of mobile");
			Assert.assertTrue("Test failed because the page does not contain This is root of mobile ", result);
			logger.info("mobile page has been verified successfully");
			
			if(result == true){
				mobileSony.click();
				logger.info("mobile product link has been clicked successfully");		
				}
		}catch(Throwable t){
			logger.error("The verifyPageAndOpenProductPage method has encountered error" +t);
			camera.takeShot("verifyPageAndOpenProductPage");
		}
		
	}

}
