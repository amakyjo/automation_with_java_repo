package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WishListPage extends AbstractPage {
	
	
	@FindBy(css=".btn-remove.btn-remove2")
	private WebElement deleteBtn;
	
	private String delMsg = "You have no items in your wishlist";

	public WishListPage(WebDriver driver) {
		super(driver);
	
	}
	
	public void verifyProductAddedToWishList(String tvType){
		try{
			logger.info("The verifyProductAddedToWishList method has started successfully");
			boolean wishListResult = driver.getPageSource().contains(tvType);
			Assert.assertTrue("Test failed because the page does not contain"+tvType, wishListResult);
			logger.info("Product added to wishlist has been verified");
		}catch(Throwable t){
			logger.error("The method verifyProductAddedToWishList has encountered error" +t);
			camera.takeShot("verifyProductAddedToWishList");
			
		}
	}
	
	public void deleteItemFromWishList(){
		
		try{
			logger.info("The deleteItemFromWishList method has started successfully");
			deleteBtn.click();
			Thread.sleep(5000);
			Alert alert = driver.switchTo().alert();
			alert.accept();
			logger.info("Product added to wishlist has been deleted successfully");
		}catch(Throwable t){
			logger.error("The method deleteItemFromWishList has encountered error" +t);
			camera.takeShot("deleteItemFromWishList");
			
		}
	}
	
	public void verifyProductIsDeletedFromWishList(){
		try{
			logger.info("The verifyProductIsDeletedFromWishList method has started successfully");
			boolean delResult = driver.getPageSource().contains(delMsg);
			Assert.assertTrue("Test failed because the page does not contain"+delMsg, delResult);
			logger.info("Product added to wishlist has been deleted successfully");
		}catch(Throwable t){
			logger.error("The method verifyProductIsDeletedFromWishList has encountered error" +t);
			camera.takeShot("verifyProductIsDeletedFromWishList");
			
		}
	}

}
