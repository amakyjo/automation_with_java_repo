package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage extends AbstractPage{

	
	@FindBy(id="qty")
	private WebElement gtyField;
	
	@FindBy(css=".button.btn-cart")
	private WebElement addToCartbtn;
	
	@FindBy(xpath="html/body/div[1]/div/div[2]/div/div[2]/div[2]/div[1]/form/div[4]/div/ul[1]/li[1]/a")
	private WebElement addToWishListLink;


	public ProductPage(WebDriver driver) {
		super(driver);
	}
	

	public ShoppingCartPage addProductToCart(String item){
		try{
			logger.info("The addProductToCart method has been initiated");
			if(item.equalsIgnoreCase("Samsung Galaxy")){
				gtyField.clear();
				gtyField.sendKeys("1");
				addToCartbtn.click();
			}
			else if(item.equalsIgnoreCase("Sony Xperia")){
				gtyField.clear();
				gtyField.sendKeys("1");
				addToCartbtn.click();
			}
			else if(item.equalsIgnoreCase("IPhone")){
				gtyField.clear();
				gtyField.sendKeys("1");
				addToCartbtn.click();
			}
			logger.info("Product has been successfully added to the cart");	
		}catch(Throwable t){
			logger.error("The addProductToCart method has encountered error" +t);
			camera.takeShot("addProductToCart");
		}
		return PageFactory.initElements(driver, ShoppingCartPage.class);
	}
	
	
	public WishListPage addProductToWishList(String item){
		try{
			logger.info("The addProductToWishList method has been initiated");
			if(item.equalsIgnoreCase("LG LCD")){
				gtyField.clear();
				gtyField.sendKeys("1");
				addToWishListLink.click();
			}
			else if(item.equalsIgnoreCase("Samsung LCD")){
				gtyField.clear();
				gtyField.sendKeys("1");
				addToWishListLink.click();
			}		
				logger.info("Product has been successfully added to wish list");	
		}catch(Throwable t){
			logger.error("The addProductToWishList method has encountered error" +t);
			camera.takeShot("addProductToWishList");
		}
		return PageFactory.initElements(driver, WishListPage.class);

	}

}
