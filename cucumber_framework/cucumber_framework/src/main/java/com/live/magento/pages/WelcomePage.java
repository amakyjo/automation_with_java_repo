package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage extends AbstractPage{
	
	public WelcomePage(WebDriver driver) {
		super(driver);

	}
	
	
	public void verifyLogin(String message){
		try{
			logger.info("The verifyLogin method has started successfully");
			boolean result = driver.getPageSource().contains(message);
			Assert.assertTrue("Test failed because the page does not contain message", result);
			logger.info("message has been verified");
		}catch(Throwable t){
			logger.error("The method verifyLogin has encountered error" +t);
			camera.takeShot("verifyLogin");
			
		}
	}


	public HomePage visitHomePage() {
		return PageFactory.initElements(driver, HomePage.class);

	
	}
	


}
