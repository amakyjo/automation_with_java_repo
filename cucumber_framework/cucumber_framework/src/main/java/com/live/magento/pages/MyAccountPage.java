package com.live.magento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends AbstractPage {
	
	@FindBy(id="email")
	private WebElement usernameField;
	
	@FindBy(id="pass")
	private WebElement passwordField;
	
	@FindBy(id="send2")
	private WebElement loginBtnField;


	public MyAccountPage(WebDriver driver) {
		super(driver);
		
	}


	public WelcomePage loginWith(String username, String password){
		try{
			logger.info("The loginWith method has started successfully");
			usernameField.sendKeys(username);
			logger.info("username has been entered successfully");
			passwordField.sendKeys(password);
			logger.info("password has been entered successfully");
			loginBtnField.click();
			logger.info("my account has been clicked");
		}catch(Throwable t){
			logger.error("The method loginWith has encountered error" +t);
			camera.takeShot("loginWith");
		}

		return PageFactory.initElements(driver, WelcomePage.class);
		
	}
}
