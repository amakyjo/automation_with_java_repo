package com.live.magento.pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.live.magento.browsers.Browsers;
import com.live.magento.utilities.Screenshot;
import com.live.magento.utilities.UrlFormatter;

public class AbstractPage {
	
	protected WebDriver driver;
	protected static Properties config = new Properties();
	protected static InputStream fis;
	protected Screenshot camera;
	protected static Logger logger = LogManager.getLogger("TestLogger");
	
	@FindBy(id="search")
	private WebElement searchID;
	
	@FindBy(css=".button.search-button")
	private WebElement searchButton;
	
	@FindBy(xpath="html/body/div[1]/div/header/div/div[3]/nav/ol/li[1]/a")
	private WebElement mobileLink;
	
	@FindBy(xpath="html/body/div[1]/div/header/div/div[3]/nav/ol/li[2]/a")
	private WebElement tvLink;
	
	
	public AbstractPage(WebDriver driver){
		this.driver = driver;
		this.camera = new Screenshot(driver);
		
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+ "\\src\\main\\resources\\testData.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			config.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

		
		
	public HomePage loadDefaultPage(){
		try{
			logger.info("The loadDefaultPage method is started correctly");
			String formatedUrl = UrlFormatter.formatUrl(config.getProperty("baseUrl"));
			logger.info("The Url has been formatted correctly");
			driver.navigate().to(formatedUrl);
			logger.info("The Web address has been loaded successfully");
		}catch(Throwable t){
			logger.error("The loadDefaultPage Method has encountered error" +t);
			camera.takeShot("loadDefaultPage");
		}
		return PageFactory.initElements(driver, HomePage.class);
	}
	

	public SearchResultPage searchForProduct(String productName) {
		try{
			logger.info("The searchForProduct method has been initiated");
			searchID.sendKeys(productName);
			logger.info("The product name to be searched has been entered successfully");
			searchButton.click();
			logger.info("The search button has been clicked");
		}catch(Throwable t){
			logger.error("The searchForProduct method has encountered error" +t);
			camera.takeShot("searchForProduct");
		}

		return PageFactory.initElements(driver, SearchResultPage.class);
		
	}
	
		
	public MobileProductPage openMobileProductPage(){
		try{
			logger.info("The openMobileProductPage method has been initiated");
			mobileLink.click();
			logger.info("Mobile link has been clicked successfully");
		}catch(Throwable t){
			logger.error("The openMobileProductPage method has encountered error" +t);
			camera.takeShot("openMobileProductPage");
		}
		return PageFactory.initElements(driver, MobileProductPage.class);
	}
	
	
	public TvProductPage openTvProductPage(){
		try{
			logger.info("The openTvProductPage method has been initiated");
			tvLink.click();
			logger.info("Tv link has been clicked successfully");
		}catch(Throwable t){
			logger.error("The openTvProductPage method has encountered error" +t);
			camera.takeShot("openTvProductPage");
		}
		return PageFactory.initElements(driver, TvProductPage.class);
	}
	
	
}
