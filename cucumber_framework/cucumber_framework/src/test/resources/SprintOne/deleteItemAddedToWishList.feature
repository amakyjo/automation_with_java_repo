@joy
Feature: Testing for deleting item added to wishlist fuctionality
  As a customer I want to delete an item added to wishlist
  so that I can check them out

  Scenario: Testing for deleting single item added to wishlist
    Given that I am logged on with username "234dotus@gmail.com" & password "password123" and added an item "Samsung LCD" to wish list
    When I delete the item "Samsung LCD" from wishlist
    Then the item "Samsung LCD" should be removed from wishlist
