@reg
Feature: Testing for adding item to cart fuctionality
  As a customer I want to search an item and add to shopping cart
  so that I can check them out


  Scenario: Testing for adding single item to cart
    Given that I am logged on with username "234dotus@gmail.com" & password "password123" and have searched for item "Samsung Galaxy"
    When I add the search item "Samsung Galaxy" to shopping cart
    Then the item "Samsung Galaxy" should exist in shopping cart
