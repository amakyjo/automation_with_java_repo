@wip
Feature: Testing of Login Functionality
  As online shoping customer
  I want to login to the application
  So I can do winter sale shopping

  Background: 
    Given that I am on the home page

  Scenario Outline: Testing of login functionality using valid username and valid password
    When I login with username as "<username>" and password as "<password>"
    Then I should see message "<message>"

    Examples: This is the data for the test above
      | username                 | password        | message                   |
      | 234dotus@gmail.com       | password123     | Donald                    |
      | 234dotus@gmail.com       | passwohghfrd123 | Invalid login or password |
      | 234dotuxxxxxxs@gmail.com | password123     | Invalid login or password |
      | 234dotuxxxxxxs@gmail.com | passwohghfrd123 | Invalid login or password |
      | 234dotus@gmail.com       |                 | Login                     |
      | 234dotuxxxxxxs@gmail.com |                 | Login                     |
      |                          | password123     | Login                     |
      |                          | passwohghfrd123 | Login                     |
      |                          |                 | Login                     |
