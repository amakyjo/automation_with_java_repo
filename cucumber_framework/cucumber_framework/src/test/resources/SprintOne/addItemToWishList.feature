@wip
Feature: Testing for adding item to wishlist fuctionality
  As a customer I want to add an item to wishlist
  so that I can check them out

  Scenario: Testing for adding single item to wishlist
    Given that Iam on the home page and I log on with username "234dotus@gmail.com" & password "password123"
    When I add the item "Samsung LCD" to wishlist
    Then the item "Samsung LCD" should exist in wishlist
