Feature: Testing of Search Functionality
  As online shopping customer
  I want to search for a product
  So I can do christmas shopping

  Scenario: Testing of search functionality using product name
    Given that I am on search page
    When I search for product "LG LCD"
    Then I should see product "LG LCD"

  Scenario: Testing of search functionality using product name
    Given that I am on search page
    When I search for product "Iphone"
    Then I should see product "Iphone"

  Scenario: Testing of search functionality using product name
    Given that I am logged on with username as "234dotus@gmail.com" and password as "password123" and landed on search page
    When I search for product "Samsung Galaxy"
    Then I should see product "Samsung Galaxy"
