package com.live.magento.steps_definitions;

import com.live.magento.helper.WorldHelper;
import com.live.magento.pages.WelcomePage;
import com.live.magento.pages.WishListPage;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddToWishListFeatureStepDefinition {
	
	private WorldHelper helper;
	private WelcomePage welcomePage;
	private WishListPage wishListPage;

	public AddToWishListFeatureStepDefinition(WorldHelper helper){
		this.helper = helper;
	}
	
	@Given("^that Iam on the home page and I log on with username \"([^\"]*)\" & password \"([^\"]*)\"$")
	public void that_Iam_on_the_home_page_and_I_log_on_with_username_password(String username, String password) throws Throwable {
		welcomePage = helper
				.getHomePage()
				.openMyAccount()
				.loginWith(username, password);
	   
	}
	
	@When("^I add the item \"([^\"]*)\" to wishlist$")
	public void i_add_the_item_to_wishlist(String productType) throws Throwable {
		
	   wishListPage = welcomePage
			   .openTvProductPage()
			   .selectTvProductType(productType)
			   .addProductToWishList(productType);
	}

	@Then("^the item \"([^\"]*)\" should exist in wishlist$")
	public void the_item_should_exist_in_wishlist(String tvType) throws Throwable {
		wishListPage.verifyProductAddedToWishList(tvType);
	  
	}
	

}
