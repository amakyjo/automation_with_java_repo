
package com.live.magento.code_hooks;

import org.openqa.selenium.TakesScreenshot;

import com.live.magento.browsers.Browsers;
import com.live.magento.pages.HomePage;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class ScenarioHooks {
	
	
	private Browsers browsers;


	public ScenarioHooks(Browsers browsers){
		this.browsers = browsers;
	}
	
	
	@Before
	public void setUpTestEnvironment(){
		
		browsers.instatiateDriver().setUpCleanDriver().loadDefaultPage(); 
	}
	
	
	@After
	public void tearDownTestEnvironment(Scenario scenario){
		if(scenario.isFailed() == true){
			//System.out.println("The test "+scenario.getStatus());
			scenario.write("The test " + scenario.getStatus());
		}else if(scenario.isFailed() == false){
			scenario.write("The test "+scenario.getStatus());
		}		
		scenario.write("The test "+scenario.getName());
		scenario.write("The test "+scenario.getStatus());
		browsers.destroyDriver();
	}
	


}
