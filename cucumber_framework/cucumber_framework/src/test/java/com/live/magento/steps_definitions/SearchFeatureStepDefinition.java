package com.live.magento.steps_definitions;

import com.live.magento.helper.WorldHelper;
import com.live.magento.pages.HomePage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.pages.WelcomePage;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchFeatureStepDefinition {
	
	private WorldHelper helper;
	private HomePage homePage;
	private SearchResultPage searchResultPage;
	private WelcomePage welcomePage;

	public SearchFeatureStepDefinition(WorldHelper helper){
		this.helper = helper;
	}

	@Given("^that I am on search page$")
	public void that_I_am_on_search_page() throws Throwable {
	   homePage = helper.getHomePage();
	}

	@When("^I search for product \"([^\"]*)\"$")
	public void i_search_for_product(String productName) throws Throwable {
	  searchResultPage = homePage.searchForProduct(productName);
	}

	@Then("^I should see product \"([^\"]*)\"$")
	public void i_should_see_product(String productName) throws Throwable {
		searchResultPage.verifyProduct(productName);
	}
	
//	@Given("^that I am logged on with username as \"([^\"]*)\" and password as \"([^\"]*)\" and landed on search page$")
//	public void that_I_am_logged_on_with_username_as_and_password_as_and_landed_on_search_page(String username, String password) throws Throwable {
//		homePage = helper.getHomePage().openMyAccount().loginWith(username, password).visitHomePage();
//	 
//	}
	    
	    
}