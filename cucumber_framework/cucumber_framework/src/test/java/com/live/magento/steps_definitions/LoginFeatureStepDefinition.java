package com.live.magento.steps_definitions;


import com.live.magento.helper.WorldHelper;
import com.live.magento.pages.AbstractPage;
import com.live.magento.pages.HomePage;
import com.live.magento.pages.MyAccountPage;
import com.live.magento.pages.WelcomePage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginFeatureStepDefinition {
	
	private WorldHelper helper;
	private MyAccountPage myAccountPage;
	private WelcomePage welcomePage;
	
	public LoginFeatureStepDefinition(WorldHelper helper){
		this.helper = helper;
	}

	@Given("^that I am on the home page$")
	public void that_I_am_on_the_home_page() throws Throwable {
		myAccountPage = helper.getHomePage().openMyAccount();
			
	}
	
	@When("^I login with username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void i_login_with_username_as_and_password_as(String username, String password) throws Throwable {
		 welcomePage = myAccountPage.loginWith(username, password);
	}

	@Then("^I should see message \"([^\"]*)\"$")
	public void i_should_see_message(String message) throws Throwable {
		welcomePage.verifyLogin(message);
	}
	

}
