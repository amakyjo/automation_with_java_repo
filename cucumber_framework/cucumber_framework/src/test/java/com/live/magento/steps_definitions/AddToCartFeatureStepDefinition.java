package com.live.magento.steps_definitions;

import com.live.magento.helper.WorldHelper;
import com.live.magento.pages.AbstractPage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.pages.ShoppingCartPage;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddToCartFeatureStepDefinition {
	
	private WorldHelper helper;

	private SearchResultPage searchResultPage;
	private ShoppingCartPage shoppingCartPage;
	
	


	public AddToCartFeatureStepDefinition(WorldHelper helper){
		 this.helper = helper;
		 
	}
	
	@Given("^that I am logged on with username \"([^\"]*)\" & password \"([^\"]*)\" and have searched for item \"([^\"]*)\"$")
	public void that_I_am_logged_on_with_username_password_and_have_searched_for_item(String username, String password, String productName) throws Throwable {
	  searchResultPage = helper
	  .getHomePage()
	  .openMyAccount()
	  .loginWith(username, password)
	  .visitHomePage()
	  .searchForProduct(productName);
	}

	@When("^I add the search item \"([^\"]*)\" to shopping cart$")
	public void i_add_the_search_item_to_shopping_cart(String productType) throws Throwable {
	    shoppingCartPage = searchResultPage
	     .openMobileProductPage()
	     .selectMobileProductType(productType)
	     .addProductToCart(productType);
	}



	@Then("^the item \"([^\"]*)\" should exist in shopping cart$")
	public void the_item_should_exist_in_shopping_cart(String mobileType) throws Throwable {
	   shoppingCartPage.verifyProductAddedToCart(mobileType);

}
	
}