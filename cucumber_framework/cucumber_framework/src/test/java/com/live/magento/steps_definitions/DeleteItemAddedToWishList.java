package com.live.magento.steps_definitions;

import com.live.magento.helper.WorldHelper;
import com.live.magento.pages.WishListPage;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DeleteItemAddedToWishList {
	
	private WorldHelper helper;
	private WishListPage wishListPage;

	public DeleteItemAddedToWishList(WorldHelper helper){
		this.helper = helper;
	}

	@Given("^that I am logged on with username \"([^\"]*)\" & password \"([^\"]*)\" and added an item \"([^\"]*)\" to wish list$")
	public void that_I_am_logged_on_with_username_password_and_added_an_item_to_wish_list(String username, String password, String productType) throws Throwable {
		wishListPage = helper
				.getHomePage()
				.openMyAccount()
				.loginWith(username, password)
				.openTvProductPage()
				.selectTvProductType(productType)
				.addProductToWishList(productType);
	   
	}

	@When("^I delete the item \"([^\"]*)\" from wishlist$")
	public void i_delete_the_item_from_wishlist(String arg1) throws Throwable {
		wishListPage.deleteItemFromWishList();
	    
	}

	@Then("^the item \"([^\"]*)\" should be removed from wishlist$")
	public void the_item_should_be_removed_from_wishlist(String arg1) throws Throwable {
	    wishListPage.verifyProductIsDeletedFromWishList();
	}
	
}
