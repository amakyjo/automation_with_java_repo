package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.live.magento.database.DbManager;

public class SearchResultPage extends AbstractPage{
	
	@FindBy(id="product-collection-image-5")
	private WebElement samsumgField;
	@FindBy(id="product-collection-image-4")
	private WebElement lcdField;
	
	public SearchResultPage(WebDriver driver) {
		super(driver);
	}

	public void verifyTheProduct(String productName) {
		try {
			boolean conditionRes = driver.getPageSource().contains(productName);
			logger.info("The condition has been determined"+ conditionRes);
			Assert.assertFalse("The Product does not exist on the application", !conditionRes);
			logger.info("The product has been found on the page");
		}catch(Exception e) {
			logger.error("The verifyTheProduct method has encountered error", e);
			camera.takeShot("verifyTheProduct");
		}		
	}
	

	public LCDTVPage selectItemToBeAddedToCart(String productName) {
		try {
			logger.info("The selectItemToBeAddedToCart method has started with: "+productName);
			if(productName.equalsIgnoreCase("Samsung LCD")){
				samsumgField.click();
			}else if(productName.equalsIgnoreCase("LG LCD")){
				lcdField.click();
			}
			logger.info("The selectItemToBeAddedToCart end successfully by adding "+productName);
		}catch(Exception e) {
			logger.error("The selectItemToBeAddedToCart method has encountered an error", e);
			camera.takeShot("selectItemToBeAddedToCart");
		}
		return PageFactory.initElements(driver, LCDTVPage.class);
	}
	
	
	

}
