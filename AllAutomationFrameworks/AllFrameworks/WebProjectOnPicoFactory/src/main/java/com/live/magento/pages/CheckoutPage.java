package com.live.magento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutPage extends AbstractPage{
	
	@FindBy(id="billing:use_for_shipping_yes")
	private WebElement shipToThisAddressLocationField;
	@FindBy(xpath ="//*[@id='billing-buttons-container']/button")
	private WebElement billingInformationContinue;
	@FindBy(xpath ="//*[@id='shipping-method-buttons-container']/button")
	private WebElement completeShippingMethodAndContinue;
	@FindBy(xpath = "//*[@id='p_method_checkmo']")
	private WebElement checkAndMoneyOrderPaymentTypeField;
	@FindBy(id="p_method_ccsave")
	private WebElement creditCardPaymentTypeField;
	@FindBy(xpath = "//*[@id='payment-buttons-container']/button")
	private WebElement continueButtonOnthePaymentTypeField;
	@FindBy(xpath = "//*[@id='review-buttons-container']/button")
	private WebElement placeOrderLocatorField;
	
	public CheckoutPage(WebDriver driver) {
		super(driver);
	}

	public CheckoutPage completeBillingInformation(String billingAddress) {
		try {
			logger.info("The completeBillingInformation method has started with: "+billingAddress);
			if(billingAddress.contains("Default Address")){
				if(shipToThisAddressLocationField.isSelected()== false){
					shipToThisAddressLocationField.click();
				}
			}else{
				
			}
			billingInformationContinue.click();
			logger.info("The completeBillingInformation end successfully by adding "+billingAddress);
		}catch(Exception e) {
			logger.error("The completeBillingInformation method has encountered an error", e);
			camera.takeShot("completeBillingInformation");
		}
		return PageFactory.initElements(driver, CheckoutPage.class);
	}

	public CheckoutPage completeShippingInformation(String shippingAddress) {
		try {
			logger.info("The completeShippingInformation method has started with: "+shippingAddress);
			if(shippingAddress.contains("Default Address")){
				logger.info("Defualt address was used and this sesction skipped as expected");
			}else if(shippingAddress.contains("New Address")){
				
			}
			logger.info("The completeShippingInformation end successfully by adding "+shippingAddress);
		}catch(Exception e) {
			logger.error("The completeShippingInformation method has encountered an error", e);
			camera.takeShot("completeShippingInformation");
		}
		return PageFactory.initElements(driver, CheckoutPage.class);
	}

	public CheckoutPage completeShippingMethod() {
		try {
			logger.info("The completeShippingMethod method has started ");
			completeShippingMethodAndContinue.click();
			logger.info("The completeShippingMethod end successfully ");
		}catch(Exception e) {
			logger.error("The completeShippingMethod method has encountered an error", e);
			camera.takeShot("completeShippingMethod");
		}
		return PageFactory.initElements(driver, CheckoutPage.class);
	}

	public CheckoutPage completePaymentInformation(String paymentType) {
		try {
			logger.info("The completePaymentInformation method has started with: "+ paymentType);
			if(paymentType.equalsIgnoreCase("Check/Money order")){
				logger.info(checkAndMoneyOrderPaymentTypeField.isEnabled());
				checkAndMoneyOrderPaymentTypeField.click();
			}else if(paymentType.contains("Credit")){
				creditCardPaymentTypeField.click();
			}
			continueButtonOnthePaymentTypeField.click();
			logger.info("The completePaymentInformation end successfully by adding "+ paymentType);
		}catch(Exception e) {
			logger.error("The completePaymentInformation method has encountered an error", e);
			camera.takeShot("completePaymentInformation");
		}
		return PageFactory.initElements(driver, CheckoutPage.class);
	}

	public OrderSummaryPage placeOrder() {
		try {
			logger.info("The placeOrder method has started");
			placeOrderLocatorField.click();
			logger.info("The placeOrder end successfully by adding ");
		}catch(Exception e) {
			logger.error("The placeOrder method has encountered an error", e);
			camera.takeShot("placeOrder");
		}
		return PageFactory.initElements(driver, OrderSummaryPage.class);
	}

}
