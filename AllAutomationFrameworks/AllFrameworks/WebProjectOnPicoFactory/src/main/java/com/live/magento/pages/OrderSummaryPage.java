package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class OrderSummaryPage extends AbstractPage{

	public OrderSummaryPage(WebDriver driver) {
		super(driver);
	}

	
	public void validateTheFollowing(String successMessage1, String successMessage2) {
		try{
			logger.info("The validateTheFollowing started with :"+successMessage1 +" and " +successMessage2);
			boolean result1 = driver.getPageSource().contains(successMessage1);
			Assert.assertTrue(successMessage1+"  does not exist", result1);
			logger.info("This is to prove that this "+successMessage1 +"has been validated");
			boolean result2 = driver.getPageSource().contains(successMessage2);
			Assert.assertTrue(successMessage2+ " does not exist", result2);
			logger.info("This is to prove that this "+successMessage2 +"has been validated");
			logger.info("The validateTheFollowing ended with :"+successMessage1 +" and " +successMessage2);
		}catch(Exception e){
			camera.takeShot("validateTheFollowing");
			logger.error("The validateTheFollowing has encountered an error: ",e);
		}
	}

}
