package com.live.magento.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LCDProductPage extends AbstractPage{

	public LCDProductPage(WebDriver driver) {
		super(driver);
	}

	public void addLCDtoCartlog() {
		try{
			logger.info("The addLCDtoCartlog has started successfully");
			driver.findElement(By.cssSelector(".button.btn-cart")).click();
			logger.info("The addLCDtoCartlog has started successfully");
		}catch(Exception e){
			camera.takeShot("addLCDtoCartlog");
			logger.debug("The addLCDtoCartlog encountered an error: ", e);
		}
		
		
	}

}
