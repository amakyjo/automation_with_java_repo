package com.live.magento.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactUsPage extends AbstractPage{
	
	@FindBy(id="name")
	private WebElement nameFieldElement;
	@FindBy(id="email")
	private WebElement emailFieldElement;
	@FindBy(id="telephone")
	private WebElement telephoneFieldElement;
	@FindBy(id="comment")
	private WebElement commentFieldElement;
	@FindBy(xpath="//*[@id='contactForm']/div[2]/button")
	private WebElement submitFieldElement;
	
	
	
	
	public ContactUsPage(WebDriver driver) {
		super(driver);
	}

	public ContactUsPage fillAndSubmitContactUsFormWithTheFollowingDetails(String name, String email, String phone,
			String comment) {
		try {			
			logger.info("The fillAndSubmitContactUsFormWithTheFollowingDetails has started with the following data: "+name +", "+email+", "+phone +" and "+comment);
			nameFieldElement.sendKeys(name);
			logger.info("Name has been entered successfully");
			emailFieldElement.sendKeys(email);
			logger.info("Email has been entered successfully");
			telephoneFieldElement.sendKeys(phone);
			logger.info("Telephone has been entered successfully");
			commentFieldElement.sendKeys(comment);
			logger.info("Comment has been entered successfully");
			submitFieldElement.click();
			logger.info("Submit has been entered successfully");
		}catch(Throwable t) {
			logger.error("The method fillAndSubmitContactUsFormWithTheFollowingDetails has encountered error ", t);
			camera.takeShot("fillAndSubmitContactUsFormWithTheFollowingDetails");
		}
//		return new ContactUsPage(driver);
		return PageFactory.initElements(driver, ContactUsPage.class);
	}

	public boolean validateSuccessMessage(String successMessage) {
		boolean result = false;
		logger.info("The validateSuccessMessage method has started successfully");
		try{
			result = driver.getPageSource().contains(successMessage);
			logger.info("The validateSuccessMessage method has ended successfully");
		}catch(Throwable t) {
			logger.error("The method validateSuccessMessage has encountered error ", t);
			camera.takeShot("validateSuccessMessage");
		}
		return result;
	}

	public boolean validatePageTitle(String pageTitle) {
			boolean result = false;
			logger.info("The validatePageTitle method has started successfully");
			try{
				result = driver.getTitle().contains(pageTitle);
				logger.info("The validatePageTitle method has ended successfully");
			}catch(Throwable t) {
				logger.error("The method validatePageTitle has encountered error ", t);
				camera.takeShot("validatePageTitle");
			}
			return result;
	}
	

}
