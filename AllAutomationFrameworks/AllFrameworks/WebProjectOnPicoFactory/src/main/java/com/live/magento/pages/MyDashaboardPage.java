package com.live.magento.pages;

import java.sql.ResultSet;

import org.openqa.selenium.WebDriver;

import com.live.magento.database.DbManager;

public class MyDashaboardPage extends AbstractPage{


	public MyDashaboardPage(WebDriver driver) {
		super(driver);
	}

	public boolean getDatabaseQueryResult(String username) {
		boolean result1 = false;
		String query = "SELECT * FROM magento.userdetails WHERE firstname = '"+username+"';";
		try {
			ResultSet rs = DbManager.runQuery(query);
			while(rs.next()) {
				String firstName = rs.getString("firstname");
				String lastName = rs.getString("lastname");
//				System.out.println(firstName);
				if(firstName.equalsIgnoreCase(username)) {
					return true;
				}else {
					return false;
				}
			}
		}catch(Throwable t) {
			logger.error("The method getDatabaseQueryResult has encountered error ", t);
			camera.takeShot("getDatabaseQueryResult");
		}
		return result1;
	}

	
	public boolean getDatabaseQueryForProfileUpdateFirstName(String firstName) {
		boolean result1 = false;
		String query = "SELECT * FROM magento.userdetails WHERE firstname = '"+firstName+"';";
		try {
			ResultSet rs = DbManager.runQuery(query);
			while(rs.next()) {
				String firstNameFromDatabase = rs.getString("firstname");
				logger.info("The first name from Database is : "+ firstNameFromDatabase);
				if(firstNameFromDatabase.equalsIgnoreCase(firstName)) {
					return true;
				}else {
					return false;
				}
			}
		}catch(Throwable t) {
			logger.error("The method getDatabaseQueryForProfileUpdateFirstName has encountered error ", t);
			camera.takeShot("getDatabaseQueryResult");
		}
		return result1;
	}
	
	
//	String lastName = rs.getString("lastname");
	
	
	public boolean getSubmissionFeedback(String successMessage) {
		boolean result1 = false;
		try {
			logger.info("The getSubmissionFeedback has started with: "+successMessage);
			result1 = driver.getPageSource().contains(successMessage);
		}catch(Throwable t) {
			logger.error("The method getSubmissionFeedback has encountered error ", t);
			camera.takeShot("getSubmissionFeedback");
		}
		return result1;
	}

	public boolean getDatabaseQueryForProfileUpdateLastName(String lastName) {
		boolean result1 = false;
		String query = "SELECT * FROM magento.userdetails WHERE lastname = '"+lastName+"';";
		try {
			ResultSet rs = DbManager.runQuery(query);
			while(rs.next()) {
				String lastNameFromDatabase = rs.getString("lastname");
				logger.info("The first name from Database is : "+ lastNameFromDatabase);
				logger.info("The email address from Database is : "+ rs.getString("email"));
				if(lastNameFromDatabase.equalsIgnoreCase(lastName)) {
					return true;
				}else {
					return false;
				}
			}
		}catch(Throwable t) {
			logger.error("The method getDatabaseQueryResult has encountered error ", t);
			camera.takeShot("getDatabaseQueryResult");
		}
		return result1;
	}


}
