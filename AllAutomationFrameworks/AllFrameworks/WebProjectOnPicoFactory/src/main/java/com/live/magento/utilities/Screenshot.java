package com.live.magento.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot {
	
	
	private WebDriver driver;
	
	public Screenshot(WebDriver driver) {
		this.driver = driver;
	}
	
	
	public void takeShot(String methodName) {
		Calendar cal = new GregorianCalendar();
		  int month = cal.get(Calendar.MONTH); //3
		  int year = cal.get(Calendar.YEAR); //2014
		  int sec = cal.get(Calendar.SECOND);
		  int min = cal.get(Calendar.MINUTE);
		  int date = cal.get(Calendar.DATE);
		  int day = cal.get(Calendar.HOUR_OF_DAY);
		  
		  String timeStamp = year+"-"+date+"-"+(month+1)+"-"+day+"-"+min+"-" +sec;
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String path = System.getProperty("user.dir");
		String path2 = "\\src\\main\\resources\\Reports\\screenshots\\";
		try {		
			FileUtils.copyFile(scrFile, new File(path + path2  + methodName +timeStamp+ ".jpeg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
