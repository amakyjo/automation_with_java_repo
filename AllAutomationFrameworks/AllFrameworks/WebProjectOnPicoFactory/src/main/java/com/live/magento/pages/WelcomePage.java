package com.live.magento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage extends AbstractPage{

	@FindBy(xpath = "//*[@id='top']/body/div[1]/div/div[2]/div/div[1]/div/div[2]/ul/li[2]/a")
	private WebElement accountInformationElementField;


	public WelcomePage(WebDriver driver) {
		super(driver);
	}
	
	
	public void verifyLogin() {
		try {
			logger.info("Username has been verified");
		}catch(Throwable t) {
			logger.error("The verify method has encountered error ", t);
			camera.takeShot("verifyLogin");
		}
		
	}


	public HomePage visitHomePage() {
		return PageFactory.initElements(driver, HomePage.class);
	}


	public EditAccountInfoPage goToAccountInformation() {
	try {
		logger.info("goToAccountInformation method started succcessfully");
		accountInformationElementField.click();
		logger.info("goToAccountInformation method ended succcessfully");
	}catch(Exception ex) {
		logger.error("goToAccountInformation method has encountered error: ",ex);
	}		
		return PageFactory.initElements(driver, EditAccountInfoPage.class);
	}

}
