package com.live.magento.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;



public class ShoppingCartPage extends AbstractPage{
	
	@FindBy(id = "postcode")
	private WebElement postCodeLocationField;
	
	@FindBy(css = ".button.btn-proceed-checkout.btn-checkout")
	private WebElement proceedToCheckOutField;	
	
	@FindBy(id="empty_cart_button")
	private WebElement samsungLocatorField;
	
	@FindBy(id="empty_cart_button")
	private WebElement lcdLocatorField;


	public ShoppingCartPage(WebDriver driver) {
		super(driver);
	}

	public boolean validateItem(String tvType) {
		boolean result = false;
		try{
			logger.info("The validateLCDTvIsAdded started with :"+tvType);
			result = driver.getPageSource().contains(tvType);
			logger.info("The validateLCDTvIsAdded ended with :"+tvType);
		}catch(Exception e){
			camera.takeShot("validateLCDTvIsAdded");
			logger.error("The validateLCDTvIsAdded has encountered an error: ",e);
		}
		return result;
	}

	public CheckoutPage proceedToCheckout(String country, String province, String zipCode) {
		try {
			logger.info("The proceedToCheckout method has started with: "+country+province+zipCode);
			WebElement countryLocation = driver.findElement(By.id("country"));
			Select selectCountry = new Select(countryLocation);
			selectCountry.selectByVisibleText(country);
			WebElement provinceLocation = driver.findElement(By.id("region_id"));
			Select selectProvince = new Select(provinceLocation);
			selectProvince.selectByVisibleText(province);
			postCodeLocationField.sendKeys(zipCode);
			proceedToCheckOutField.click();
			logger.info("The proceedToCheckout end successfully by adding "+country+province+zipCode);
		}catch(Exception e) {
			logger.error("The proceedToCheckout method has encountered an error", e);
			camera.takeShot("proceedToCheckout");
		}
		return PageFactory.initElements(driver, CheckoutPage.class);
	}



	public ShoppingCartPage deleteProductFromShoppingCartWithoutChangingMyMind(String productName) {
		try {
			logger.info("The deleteProductFromShoppingCartWithoutChangingMyMind method has started with: "+productName);
			if(productName.equalsIgnoreCase("Samsung LCD")){
				samsungLocatorField.click();
				logger.info("The "+productName+" has been successfully deleted");
			}else if(productName.equalsIgnoreCase("LG LCD")){
				lcdLocatorField.click();
				logger.info("The "+productName+" has been successfully deleted");
			}
			logger.info("The deleteProductFromShoppingCartWithoutChangingMyMind end successfully by adding "+productName);
		}catch(Exception e) {
			logger.error("The deleteProductFromShoppingCartWithoutChangingMyMind method has encountered an error", e);
			camera.takeShot("deleteProductFromShoppingCartWithoutChangingMyMind");
		}
		return PageFactory.initElements(driver, ShoppingCartPage.class);
		
	}

}
