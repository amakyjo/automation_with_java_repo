package com.live.magento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateAccountPage extends AbstractPage{
	@FindBy(id="firstname")
	private WebElement firstNameFieldElement;
	@FindBy(id="lastname")
	private WebElement lastNameFieldElement;
	@FindBy(id="email_address")
	private WebElement emailFieldElement;
	@FindBy(id="password")
	private WebElement passwordFieldElement;
	@FindBy(id="confirmation")
	private WebElement confirmPasswordFieldElement;
	@FindBy(xpath="//*[@id='form-validate']/div[2]/button")
	private WebElement submitFieldElement;

	public CreateAccountPage(WebDriver driver) {
		super(driver);
	}

	public MyDashaboardPage completeTheAccountFormAndSubmit(String firstName, String lastName, String emailAddress, String password,
			String confirmPassword) {
		try {
			logger.info("The completeTheAccountFormAndSubmit has started with the following data: "+firstName +", "+lastName+", "+emailAddress +" and "+password);
			firstNameFieldElement.sendKeys(firstName);
			logger.info("firstName has been entered successfully");
			lastNameFieldElement.sendKeys(lastName);
			logger.info("lastName has been entered successfully");
			emailFieldElement.sendKeys(emailAddress);
			logger.info("Email has been entered successfully");
			passwordFieldElement.sendKeys(password);
			logger.info("password has been entered successfully");
			confirmPasswordFieldElement.sendKeys(confirmPassword);
			logger.info("confirmPassword has been entered successfully");
			submitFieldElement.click();
			logger.info("Submit has been entered successfully");
		}catch(Throwable t) {
			logger.error("The method completeTheAccountFormAndSubmit has encountered error ", t);
			camera.takeShot("completeTheAccountFormAndSubmit");
		
		}
		return PageFactory.initElements(driver, MyDashaboardPage.class);
	}
	
	

}
