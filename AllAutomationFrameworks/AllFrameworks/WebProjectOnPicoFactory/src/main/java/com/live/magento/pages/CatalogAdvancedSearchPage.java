package com.live.magento.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CatalogAdvancedSearchPage extends AbstractPage{
	
	@FindBy(id="name")
	private WebElement fillNameElement;
	
	@FindBy(id="description")
	private WebElement descriptionElement;
	
	@FindBy(id="short_description")
	private WebElement shortDescriptionElement;
	
	@FindBy(id="sku")
	private WebElement fillSkuElement;
	
	@FindBy(id="price")
	private WebElement priceElement;
	
	@FindBy(id="price_to")
	private WebElement priceToElement;
	
	@FindBy(xpath = "//*[@id='tax_class_id']/option[1]")
	private WebElement noneTaxClassElement;
	@FindBy(xpath = "//*[@id='tax_class_id']/option[2]")
	private WebElement taxableGoodsTaxClassElement;
	@FindBy(xpath = "//*[@id='tax_class_id']/option[3]")
	private WebElement shippingTaxClassElement;
	
	@FindBy(xpath = "//*[@id='form-validate']/div[2]/button")
	private WebElement clickOnSearchElement;
	
	
	
	public CatalogAdvancedSearchPage(WebDriver driver) {
		super(driver);
		
	}

	public SearchResultsPage completeAdvancedSearchForm(String name, String description, String shortDescription, String sKU,
			String price, String priceTo, String taxClass) {
		try {
			logger.info("completeAdvancedSearchForm started execution successfully");
			fillNameElement.sendKeys(name);
			logger.info( name +" was enetered successfully");
			descriptionElement.sendKeys(description);
			logger.info(description + " was enetered successfully");
			shortDescriptionElement.sendKeys(shortDescription);
			logger.info(shortDescription + " was enetered successfully");
			fillSkuElement.sendKeys(sKU);
			logger.info(sKU + " was enetered successfully");
			priceElement.sendKeys(price);
			logger.info(price + " was enetered successfully");
			priceToElement.sendKeys(priceTo);
			logger.info(priceTo + " was enetered successfully");
			if(taxClass.toLowerCase().contains("none")) {
				noneTaxClassElement.click();
				logger.info(taxClass + " was enetered successfully");
			}else if(taxClass.toLowerCase().contains("taxable goods")) {
				taxableGoodsTaxClassElement.click();
				logger.info(taxClass + " was enetered successfully");
			}else if(taxClass.toLowerCase().contains("shipping")) {
				shippingTaxClassElement.click();
				logger.info(taxClass + " was enetered successfully");
			}else {
				logger.info(taxClass + " was enetered successfully");
				throw new NoSuchFieldException("Such element does not exist on the tax class panel");				
			}				
			
			logger.info(taxClass + " was enetered successfully");
			
			clickOnSearchElement.click();
			logger.info("clickOnSearchElement was successful");
			logger.info("completeAdvancedSearchForm finished execution successfully");
			
		}catch(Throwable t) {
			logger.error("completeAdvancedSearchForm encountered error", t);
			camera.takeShot("completeAdvancedSearchForm");
		}
			return PageFactory.initElements(driver, SearchResultsPage.class );
	}

}
