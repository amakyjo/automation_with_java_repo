package com.live.magento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class OrderAndReturnPage extends AbstractPage{
	@FindBy(id="oar_order_id")
	private WebElement orderIDFieldElement;
	@FindBy(id="oar_billing_lastname")
	private WebElement billingLastNameFieldElement;
	@FindBy(id="quick_search_type_id")
	private WebElement findOrderByFieldElement;
	@FindBy(id="oar_email")
	private WebElement emailAddressFieldElement;
	@FindBy(xpath="//*[@id='oar_widget_orders_and_returns_form']/div[2]/button")
	private WebElement trackOrderElement;
	@FindBy(id="oar_zip")
	private WebElement zipCodeAddressFieldElement;

	public OrderAndReturnPage(WebDriver driver) {
		super(driver);
	}

	public OrderAndReturnPage completeForm(String orderID, String billingLastName, String findOrderBy, String address) {
		try {
			logger.info("The completeForm has started with the following data: "+orderID +", "+billingLastName+", "+findOrderBy +" and "+address);
			orderIDFieldElement.sendKeys(orderID);
			logger.info("orderID has been entered successfully");
			billingLastNameFieldElement.sendKeys(billingLastName);
			logger.info("billingLastName has been entered successfully");
			Select selectOrderBy = new Select(findOrderByFieldElement);
			selectOrderBy.selectByVisibleText(findOrderBy);
			logger.info("findOrderBy has been entered successfully");
			if(findOrderBy.contains("Email")) {
				emailAddressFieldElement.sendKeys(address);
				logger.info("emailAddress has been entered successfully");
			}else if(findOrderBy.contains("ZIP"))	
				zipCodeAddressFieldElement.sendKeys(address);
			logger.info("Zip Code has been entered successfully");
		}catch(Throwable t) {
			logger.error("The method completeForm has encountered error ", t);
			camera.takeShot("completeForm");
		}
		return PageFactory.initElements(driver, OrderAndReturnPage.class);
	}




	public OrderStatusPage trackOrder() {
		try {
			logger.info("The trackOrder has started");
			trackOrderElement.click();
			logger.info("trackOrder has been opened successfully");
		}catch(Throwable t) {
				logger.error("The method trackOrder has encountered error ", t);
				camera.takeShot("trackOrder");
		}
		return PageFactory.initElements(driver, OrderStatusPage.class);
	}
	
}
