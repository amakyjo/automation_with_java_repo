package com.live.magento.pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.live.magento.utilities.Screenshot;

public class AbstractPage {
	
	protected  WebDriver driver;
	protected Screenshot camera;
	protected static Properties config = new Properties();
	protected static InputStream fis;
	protected static Logger logger = LogManager.getLogger("TestLogger");
	
	
	
	//Page Object has three concepts
	

	@FindBy(id="search")
	private WebElement searchButtonField;
	@FindBy(css=".button.search-button")
	private WebElement searchButton;

	
	public AbstractPage(WebDriver driver) {
		this.driver = driver;
		this.camera = new Screenshot(driver);
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+"/src/main/resources/testDataRepo.properties");
			logger.info("Path for the Object repository has been loaded successfully");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			config.load(fis);
			logger.info("config.load(fis) has been loaded successfully for browser details");
		} catch (IOException e) {
			e.printStackTrace();
		} 
		logger.info("Loaded the Locator database property file");
	}

	
	
	
	public HomePage loadDefaultPage() {
		try {
			driver.navigate().to(config.getProperty("base_url"));
			logger.info("The web Address has been loaded successfully");
		}catch(Exception e) {
			logger.error("The loadDefaultPage method has eencountered error "+e);
			camera.takeShot("loadDefaultPage");
		}
		return PageFactory.initElements(driver, HomePage.class);
	}
	
	



	public SearchResultPage searchForProduct(String productName) {		
		try {
			searchButtonField.sendKeys(productName);
			logger.info("The product " +productName+ " has been populated successfully");
			searchButton.click();
			logger.info("The search button has been clicked successfully");
		}catch(Throwable t) {
			logger.error("The searchForProduct has encountered error", t);
			camera.takeShot("searchForProduct");
		}
		return PageFactory.initElements(driver, SearchResultPage.class);
	}


	public void showTVs() {
		try{
			logger.info("The method showTVs started successfully");
			driver.findElement(By.partialLinkText("TV")).click();
			logger.info("The showTVs executed successfully");
		}catch(Exception e){
			camera.takeShot("showTVs");
			logger.error("The showTvs method encountered error", e);
		}
	}
	

	

}
