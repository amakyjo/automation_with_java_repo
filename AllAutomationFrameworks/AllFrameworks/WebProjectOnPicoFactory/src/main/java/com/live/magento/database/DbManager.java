package com.live.magento.database;

import java.sql.*;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class DbManager {
	
	
	
	//MYSQL DATABASE DETAILS
	public static String mysqldriver="com.mysql.jdbc.Driver";
	public static String mysqluserName = "root";
	public static String mysqlpassword = "admin";
	private static String databaseName = "magento";
	public static String mysqlurl = "jdbc:mysql://localhost:3306/"+databaseName;
	private static Connection conn = null;
	
	

		


//		Database Connection Code
	public static void setMysqlDbConnection() throws SQLException, ClassNotFoundException, AddressException, MessagingException
	    {
	    try{ 
	        Class.forName (mysqldriver).newInstance();
	        conn = DriverManager.getConnection(mysqlurl, mysqluserName, mysqlpassword);
	        if(!conn.isClosed())
				System.out.println("Successfully connected to MySQL server");
	    }catch (Exception e) {
	        System.err.println ("Cannot connect to database server");
	    }
	}
		
//	Database Query Method
	public static ResultSet runQuery(String query) throws SQLException{
			Statement St = conn.createStatement();
			ResultSet rs = St.executeQuery(query);			
			return rs;
	}


		
//  Database Closing code
	public static void shutDownDatabase() throws SQLException{
	    if (conn != null) {
	    		 try {
					conn.close();
					System.out.println("MySQL Database closed on the first Attempt");
	    		 } catch (Exception e) {
					e.printStackTrace();
	    		 }
	    }else{
	    	System.out.println("Database closed on second attempt");
	    }
	}
		
		
		
		
		
	
		

}

