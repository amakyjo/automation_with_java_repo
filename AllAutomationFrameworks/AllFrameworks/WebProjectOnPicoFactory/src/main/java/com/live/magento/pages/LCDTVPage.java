package com.live.magento.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LCDTVPage extends AbstractPage {
	
	@FindBy(css=".link-wishlist")
	private WebElement addSamsumgWishLIstField;
	@FindBy(css=".button.btn-cart")
	private WebElement addsamsungWebElementField;
	@FindBy(css=".button.btn-cart")
	private WebElement addlcdWebElementField;
	@FindBy(css=".link-wishlist")
	private WebElement addLgWishLIstField;
	
	
	public LCDTVPage(WebDriver driver) {
		super(driver);
		
	}





	public ShoppingCartPage addToCart(String productName) {
		try {
			logger.info("The addToCart method has started with: "+productName);
			if(productName.equalsIgnoreCase("Samsung LCD")){
				addsamsungWebElementField.click();
				logger.info("The "+productName+" has been added successfully");
			}else if(productName.equalsIgnoreCase("LG LCD")){
				addlcdWebElementField.click();
				logger.info("The "+productName+" has been added successfully");
			}
			logger.info("The addToCart end successfully by adding "+productName);
		}catch(Exception e) {
			logger.error("The addToCart method has encountered an error", e);
			camera.takeShot("addToCart");
		}
		return PageFactory.initElements(driver, ShoppingCartPage.class);
		
	}

	public MyWishListPage addToWishList(String productName) {
		try {
			logger.info("The addToWishList method has started with: "+productName);
			if(productName.equalsIgnoreCase("Samsung LCD")){
				addSamsumgWishLIstField.click();
				logger.info("The "+productName+" has been added successfully");
			}else if(productName.equalsIgnoreCase("LG LCD")){
				addLgWishLIstField.click();
				logger.info("The "+productName+" has been added successfully");
			}
			logger.info("The addToWishList end successfully by adding "+productName);
		}catch(Exception e) {
			logger.error("The addToWishList method has encountered an error", e);
			camera.takeShot("addToWishList");
		}
		return PageFactory.initElements(driver, MyWishListPage.class);
	}
	
	
	

}
