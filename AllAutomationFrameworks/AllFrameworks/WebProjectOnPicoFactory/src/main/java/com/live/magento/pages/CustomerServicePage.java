package com.live.magento.pages;

import org.openqa.selenium.WebDriver;

public class CustomerServicePage extends AbstractPage{

	public CustomerServicePage(WebDriver driver) {
		super(driver);
	}

	public boolean validateUrl(String pageUrl) {
		boolean result = false;
		try {
			logger.info("The validateUrl has started successfully");
			result = driver.getCurrentUrl().contains(pageUrl);
			logger.info("The validateUrl has ended successfully");
		}catch(Throwable t) {
			camera.takeShot("validateUrl");
			logger.error("The validateUrl has encountered error", t);			
		}
		return result;
	}

	
	public boolean validatePageTitle(String pageTitle) {
		boolean result = false;
		try {
			logger.info("The validatePageTitle has started successfully");
			result = driver.getTitle().contains(pageTitle);
			logger.info("The validatePageTitle has ended successfully");
		}catch(Throwable t) {
			camera.takeShot("validatePageTitle");
			logger.error("The validatePageTitle has encountered error", t);			
		}
		return result;
	}
	
	public boolean validateMessage(String message) {
		boolean result = false;
		try {
			Thread.sleep(6000);
			logger.info("The validateMessage has started successfully");
			result = driver.getPageSource().contains(message);
			logger.info("The validateMessage has ended successfully");
		}catch(Throwable t) {
			camera.takeShot("validateMessage");
			logger.error("The validateMessage has encountered error", t);			
		}
		return result;
	}
}
