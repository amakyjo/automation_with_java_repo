package com.live.magento.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EditAccountInfoPage extends AbstractPage{
	@FindBy(id="firstname")
	private WebElement firstNameElementField;
	@FindBy(id="lastname")
	private WebElement lastNameElementField;
	@FindBy(id="email")
	private WebElement emailAddressElementField;
	@FindBy(xpath="//*[@id='form-validate']/div[3]/button")
	private WebElement saveElementField;

	public EditAccountInfoPage(WebDriver driver) {
		super(driver);
	}

	public EditAccountInfoPage editAccountWith(String firstName, String lastName, String emailAddress) {
		try {
			logger.info("editAccountWith method started succcessfully");
			firstNameElementField.clear();
			firstNameElementField.sendKeys(firstName);
			logger.info(firstName+ "has been entered succcessfully");
			lastNameElementField.clear();
			lastNameElementField.sendKeys(lastName);
			logger.info(lastName+ "has been entered succcessfully");
			emailAddressElementField.clear();
			emailAddressElementField.sendKeys(emailAddress);
			logger.info(emailAddress+ "has been entered succcessfully");
		}catch(Exception ex) {
			logger.error("editAccountWith method has encountered error: ",ex);
		}		
			return PageFactory.initElements(driver, EditAccountInfoPage.class);
	}

	public MyDashaboardPage saveNewDetails() {
		try {
			logger.info("saveNewDetails method started succcessfully");
			saveElementField.click();
			logger.info("Clicked succcessfully");			
		}catch(Exception ex) {
			logger.error("saveNewDetails method has encountered error: ",ex);
		}		
			return PageFactory.initElements(driver, MyDashaboardPage.class);
		
	}
	
	

}
