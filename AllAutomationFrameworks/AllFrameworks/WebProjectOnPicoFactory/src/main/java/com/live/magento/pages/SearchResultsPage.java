package com.live.magento.pages;

import org.openqa.selenium.WebDriver;

public class SearchResultsPage extends AbstractPage{

	public SearchResultsPage(WebDriver driver) {
		super(driver);
		
	}

	public boolean validateProductInfo(String itemName) {	
		boolean result = false;
		try {
			logger.info("validateProductInfo started execution successfully");
			result = driver.getPageSource().contains(itemName);
			logger.info("validateProductInfo executed successfully");
		}catch(Throwable t) {
			logger.error("validateProductInfo encountered error " , t);
			camera.takeShot("validateProductInfo");
		}
		return result;
	}

}
