package com.live.magento.pages;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends AbstractPage{
	@FindBy(xpath= "//*[@id='top']/body/div[1]/div/div[3]/div/div[4]/ul/li/a")
	private WebElement myAccountField;
	
	@FindBy(xpath="//*[@id='top']/body/div[1]/div/div[3]/div/div[2]/ul/li[2]/a")
	private WebElement openContactUsFormElement;
	@FindBy(xpath = "//*[@id='top']/body/div/div/div[3]/div/div[4]/ul/li[2]/a")
	private WebElement openOrderAndReturnFormElement;
	
	@FindBy(xpath = "//*[@id='top']/body/div[1]/div/div[3]/div/div[2]/ul/li[3]/a")
	private WebElement goToCustomerServiceElement;
	
	@FindBy(xpath = "//*[@id='top']/body/div[1]/div/div[3]/div/div[3]/ul/li[3]/a")
	private WebElement clickOnAdvancedSearchElement;
	
	
	public HomePage(WebDriver driver) {
		super(driver);
	}

	

	
	
	public MyAccountPage openMyAccount() {
		try {
			logger.info("The openMyAccount has started");
			myAccountField.click();
		logger.info("Account has been opened successfully");
		}catch(Throwable t) {
			logger.error("The method openMyAccount has encountered error ", t);
			camera.takeShot("openMyAccount");
		}
		return PageFactory.initElements(driver, MyAccountPage.class);
	}





	public ContactUsPage openContactUsForm() {
		try {
			logger.info("The openContactUsForm has started");
			openContactUsFormElement.click();
			logger.info("Contact Form has been opened successfully");
			}catch(Throwable t) {
				logger.error("The method openContactUsForm has encountered error ", t);
				camera.takeShot("openContactUsForm");
			}
		return PageFactory.initElements(driver, ContactUsPage.class);
	}





	public OrderAndReturnPage goToOrderAAndReturn() {
		try {
			logger.info("The goToOrderAAndReturn has started");
			openOrderAndReturnFormElement.click();
			logger.info("Order and Return Form has been opened successfully");
		}catch(Throwable t) {
			logger.error("The method goToOrderAAndReturn has encountered error ", t);
			camera.takeShot("goToOrderAAndReturn");
		}		
		return PageFactory.initElements(driver, OrderAndReturnPage.class);
	}





	public CustomerServicePage goToCustomerService() {
		try {
			logger.info("goToCustomerService started execution");
			goToCustomerServiceElement.click();
			logger.info("goToCustomerService executed successfully");
		}catch(Throwable t) {
			logger.error("goToCustomerService encountered error" + t);
			camera.takeShot("goToCustomerService");
		}
		return PageFactory.initElements(driver, CustomerServicePage.class);
	}





	public CatalogAdvancedSearchPage clickOnAdvancedSearch() {
		try {
		logger.info("clickOnAdvancedSearch started execution");
		clickOnAdvancedSearchElement.click();
		logger.info("clickOnAdvancedSearch executed successfully");
		}catch(Throwable t) {
			logger.error("clickOnAdvancedSearch encountered error" + t);
			camera.takeShot("clickOnAdvancedSearch");
		}
		return PageFactory.initElements(driver, CatalogAdvancedSearchPage.class);
		
		
	}





}
