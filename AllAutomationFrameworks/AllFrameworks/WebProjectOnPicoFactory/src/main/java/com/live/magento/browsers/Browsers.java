package com.live.magento.browsers;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.PageFactory;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.live.magento.database.DbManager;
import com.live.magento.pages.AbstractPage;
import com.live.magento.utilities.Screenshot;
import com.live.magento.utilities.SendMail;

import cucumber.api.Scenario;




public class Browsers {
	

	protected static WebDriver driver;
//	protected static RemoteWebDriver driver;
	protected static Properties config = new Properties();
	protected static InputStream fis;
	protected static Logger logger = LogManager.getLogger("TestLogger");
	private String systemPAth;

	public Browsers instatiateDriver(){
		systemPAth = System.getProperty("user.dir");
		if(driver == null){
			logger.info("The initializeDriver method has started execution as the browser is null");			
			try {
				fis = new FileInputStream(systemPAth+"/src/main/resources/testData.properties");
				logger.info("Path for the Object repository has been loaded successfully");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			try {
				config.load(fis);
				logger.info("config.load(fis) has been loaded successfully for browser details");
			} catch (IOException e) {
				e.printStackTrace();
			} 
			logger.info("Loaded the Locator database property file");
			
			if(config.getProperty("browser").equals("firefox")){
				FirefoxProfile profile = new FirefoxProfile();
				profile.setPreference("extensions.firebug.currentVersion", "2.0.13");
				driver = new FirefoxDriver(profile);
				logger.info("Executed test on the Firefox Browser");	
			}else if(config.getProperty("browser").equals("iexplorer")){
				System.setProperty("webdriver.ie.driver", systemPAth +BrowserPath.INTERNETEXPLORER);
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
				caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				caps.setCapability("nativeEvents", false);
				driver = new InternetExplorerDriver(caps);
				logger.info("Executed test on the Internet Exprorer Browser");							
			}else if(config.getProperty("browser").equals("chrome")){
				System.setProperty("webdriver.chrome.driver", systemPAth + BrowserPath.CHROME);
				driver = new ChromeDriver();
				logger.info("Executed test on the Chrome Browser");	
			}else if(config.getProperty("browser").equals("ghost_html")){
				if(config.getProperty("htmlType").equals("chrome")){
					driver = new HtmlUnitDriver(BrowserVersion.CHROME);
				}else if(config.getProperty("htmlType").equals("firefoxv38")){
					driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_38);
				}else if(config.getProperty("htmlType").equals("iexplorev11")){
					driver = new HtmlUnitDriver(BrowserVersion.INTERNET_EXPLORER_11);
				}else if(config.getProperty("htmlType").equals("iexplorev9")){
					driver = new HtmlUnitDriver(BrowserVersion.INTERNET_EXPLORER_8);
				}
				((HtmlUnitDriver)driver).setJavascriptEnabled(true);
				logger.info("Executed test on the HTML Headless Browser");
			}else if(config.getProperty("browser").equals("safari")){
				System.setProperty("webdriver.safari.driver", systemPAth +"BrowserPath.SAFARI");
				driver = new SafariDriver();
				logger.info("Executed test on the Safari Browser");	
			}else if(config.getProperty("browser").equals("ghostphantomjs")){
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setJavascriptEnabled(true);
			    capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, systemPAth +"BrowserPath.PHANTOMJS");
		        driver = new PhantomJSDriver(capabilities);
				logger.info("Executed test on the PhantomJS Headless Browser");	
			}else{
				logger.info("There is no such browser in this application");
				throw new IllegalStateException("There is no such browser in this application");
			}		
		}
		return new Browsers();
	}
	
	
	public AbstractPage setUpCleanDriver(){
		try{
			driver.manage().window().maximize();
			logger.info("Window has been maximized");
			driver.manage().deleteAllCookies();
			logger.info("All cookies are cleared and Browser is clean");
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			logger.info("Implicit wait now applies");
		}catch(Exception e){
			logger.info("Unable to setup a clean new browser");
		}		
		return PageFactory.initElements(driver, AbstractPage.class);
	}
	
	
	public Browsers sendEmail(){
		try {
			logger.info("About to send emails ...");
			SendMail mail = new SendMail();
			mail.postMail(SendMail.emailList, SendMail.emailSubjectTxt, SendMail.emailMsgTxt, SendMail.emailFromAddress); //SendMail.emailFromAddress
			logger.info("Email has been sent out to various managers");
		} catch (MessagingException e) {
			logger.error("Unable to send email out to everyone becuase of the following error message ", e);
		}
		return new Browsers();
	}
	
//	Cucumber ScreenCapture!!!
	public void screenCapture(Scenario scenario) {
        scenario.write("The Scenario name is "+scenario.getName()+" and the status is "+scenario.getStatus());
		if (scenario.isFailed()){
			
			Screenshot sc = new Screenshot(driver);
			sc.takeShot(scenario.getName());
			
			logger.error("Scenario failed! Browser: "  + " Taking screenshot...");
            scenario.write("Current Page URL is: " + driver.getCurrentUrl());
            try {
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
            	logger.error(somePlatformsDontSupportScreenshots.getMessage());
            }
		}
	}
	
	public void setupDatabaseConnection(){
		System.out.println("Starting up the Database ...");
		try {
			DbManager.setMysqlDbConnection();
		} catch (ClassNotFoundException | SQLException | MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void closeDatabaseConnection(){
		System.out.println("Stopping the Database ...");
		try {
			DbManager.shutDownDatabase();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void destroyDriver(){
		try {
			driver.quit();
			logger.info("The browser has now quit");
			driver = null;
			logger.info("The browser has no value anymore");
		}catch(Throwable t) {
			logger.error("The tearDown has encountered error", t);
		}
	}
	
	

	
	
	

	

}
