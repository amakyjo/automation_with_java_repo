package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyWishListPage extends AbstractPage{
	

	@FindBy(css = ".btn-remove.btn-remove2")
	private WebElement samsungLocatorField;
	@FindBy(css = ".btn-remove.btn-remove2")
	private WebElement lcdLocatorField;

	public MyWishListPage(WebDriver driver) {
		super(driver);
	}

	public boolean validateItemModel(String itemModel) {
		boolean itemModelResult = false;
		try{
			logger.info("The validateItemModel started with : "+itemModel);
			itemModelResult = driver.getPageSource().contains(itemModel);
			logger.info("This is to prove that this "+itemModel +"has been validated");
		}catch(Exception e){
			camera.takeShot("validateItemModel");
			logger.error("The validateItemModel has encountered an error: ",e);
		}
		return itemModelResult;
	}
	
	public boolean validateItemAmount( String itemAmount) {
		boolean itemAmountResult = false;
		try{
			logger.info("The validateItemAmount started with : " +itemAmount);
			itemAmountResult = driver.getPageSource().contains(itemAmount);
			logger.info("This is to prove that this "+itemAmount +"has been validated");
		}catch(Exception e){
			camera.takeShot("validateItemAmount");
			logger.error("The validateItemAmount has encountered an error: ",e);
		}
		return itemAmountResult;
	}
	

	public MyWishListPage deleteProductFromWishListWithoutChangingMyMind(String productName) {		
		try {
			logger.info("The deleteProductFromWishListWithoutChangingMyMind method has started with: "+productName);
			if(productName.equalsIgnoreCase("Samsung LCD")){
				samsungLocatorField.click();
				Alert alert = driver.switchTo().alert();
				alert.accept();
			}else if(productName.equalsIgnoreCase("LG LCD")){
				lcdLocatorField.click();
				Alert alert = driver.switchTo().alert();
				alert.accept();
			}
			logger.info("The deleteProductFromWishListWithoutChangingMyMind end successfully by adding "+productName);
		}catch(Exception e) {
			logger.error("The deleteProductFromWishListWithoutChangingMyMind method has encountered an error", e);
			camera.takeShot("deleteProductFromWishListWithoutChangingMyMind");
		}
		return PageFactory.initElements(driver, MyWishListPage.class);
	}
	

	public void validateTheFollowingInTheWishListCartDoesNotExist(String itemModel, String itemAmount) {
		try{
			logger.info("The validateTHeFollowingInTheWishListCart started with :"+itemModel +" and " +itemAmount);
			boolean result1 = driver.getPageSource().contains(itemModel);
			Assert.assertTrue(itemModel+"  does exist", !result1);
			logger.info("This is to prove that this "+itemModel +"has been validated");
			boolean result2 = driver.getPageSource().contains(itemAmount);
			Assert.assertTrue(itemAmount+ " does exist", !result2);
			logger.info("This is to prove that this "+itemAmount +"has been validated");
			logger.info("The validateTHeFollowingInTheWishListCart ended with :"+itemModel +" and " +itemAmount);
		}catch(Exception e){
			camera.takeShot("validateTHeFollowingInTheWishListCart");
			logger.error("The validateTHeFollowingInTheWishListCart has encountered an error: ",e);
		}		
	}

	public MyWishListPage attemptToDeleteItemFromWishListAndLaterChangeMyMind(String productName) {
		try {
			logger.info("The attemptToDeleteItemFromWishListAndLaterChangeMyMind method has started with: "+productName);
			if(productName.equalsIgnoreCase("Samsung LCD")){
				samsungLocatorField.click();
				Alert alert = driver.switchTo().alert();
				alert.dismiss();
			}else if(productName.equalsIgnoreCase("LG LCD")){
				lcdLocatorField.click();
				Alert alert = driver.switchTo().alert();
				alert.dismiss();
			}
			logger.info("The attemptToDeleteItemFromWishListAndLaterChangeMyMind end successfully by adding "+productName);
		}catch(Exception e) {
			logger.error("The attemptToDeleteItemFromWishListAndLaterChangeMyMind method has encountered an error", e);
			camera.takeShot("attemptToDeleteItemFromWishListAndLaterChangeMyMind");
		}
		return PageFactory.initElements(driver, MyWishListPage.class);
	}
	
	
	

}
