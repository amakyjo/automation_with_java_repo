package com.live.magento.pages;

import org.openqa.selenium.WebDriver;

public class OrderStatusPage extends AbstractPage{

	public OrderStatusPage(WebDriver driver) {
		super(driver);
	}

	public boolean validateResult(String status) {
		boolean answer = false;
		try {
			logger.info("Validate status method started successfully");
			answer = driver.getPageSource().contains(status);
		}catch(Throwable t) {
			logger.info("Validate status method started successfully", t);
		}
		return answer;
	}

}
