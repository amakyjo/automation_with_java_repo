package com.live.magento.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends AbstractPage{
	@FindBy(id="email")
	private WebElement usernameField;
	@FindBy(id="pass")
	private WebElement passwordFIeld;
	@FindBy(id="send2")
	private WebElement loginButtonField;	
	@FindBy(xpath = "//*[@id='login-form']/div/div[1]/div[2]/a")
	private WebElement openCreatAnAccountElement;

	public MyAccountPage(WebDriver driver) {
		super(driver);
	}
	

	public WelcomePage loginWith() {
		try {
			usernameField.sendKeys(config.getProperty("username"));
			logger.info("Username has been entered successfully");
			passwordFIeld.sendKeys(config.getProperty("password"));
			logger.info("Password has been entered successfully");
			loginButtonField.click();
			logger.info("Login button has been clicked successfully");
			camera.takeShot("openMyAccount");
		}catch(Exception e) {
			logger.error("The loginWith method has encountered an error", e);
			camera.takeShot("loginWith");
		}
		return PageFactory.initElements(driver, WelcomePage.class);
	}


	
	
	public WelcomePage loginWith(String username, String password) {
		try {
			usernameField.sendKeys(username);
			logger.info("Username has been entered successfully");
			passwordFIeld.sendKeys(password);
			logger.info("Password has been entered successfully");
			loginButtonField.click();
			logger.info("Login button has been clicked successfully");
		}catch(Exception e) {
			logger.error("The loginWith method has encountered an error", e);
			camera.takeShot("loginWith");
		}
		return PageFactory.initElements(driver, WelcomePage.class);
	}
	
	

	public CreateAccountPage createAnAccount() {
		try {
			logger.info("The createAnAccount has started");
			openCreatAnAccountElement.click();
			logger.info("Create an account has been opened successfully");
		}catch(Throwable t) {
			logger.error("The method createAnAccount has encountered error ", t);
			camera.takeShot("createAnAccount");
		}
		return PageFactory.initElements(driver, CreateAccountPage.class);
	}

}
