//package com.live.magento.browsers;
//
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//
//import java.sql.SQLException;
//import java.util.Properties;
//import java.util.concurrent.TimeUnit;
//
//import javax.mail.MessagingException;
//
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriverException;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriverService;
////import org.openqa.selenium.remote.DesiredCapabilities;
////import org.openqa.selenium.remote.RemoteWebDriver;
//import org.openqa.selenium.safari.SafariDriver;
//import org.openqa.selenium.support.PageFactory;
//
//import com.gargoylesoftware.htmlunit.BrowserVersion;
//import com.live.magento.database.DbManager;
//import com.live.magento.pages.AbstractPage;
//import com.live.magento.utilities.SendMail;
//
//
//
//import java.net.MalformedURLException;
//import java.net.URL;
//
//import cucumber.api.Scenario;
//
//public class SauceLabBrowsers {
//	
//
//	protected static Logger logger = LogManager.getLogger("TestLogger");
////	protected static WebDriver driver;
//	protected static RemoteWebDriver driver;
//	public static final String USERNAME = "234dotus200045";
//	public static final String ACCESS_KEY = "7b958b8c-8e88-4d25-845f-d017014cb0fe";
//	public static final String URL = "http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
//
//
//	public SauceLabBrowsers instatiateDriver(){
//		
//		 DesiredCapabilities caps = DesiredCapabilities.chrome();
//		    caps.setCapability("platform", "Windows XP");
//		    caps.setCapability("version", "43.0");
//		    caps.setCapability("name", "Batch2015");
//		    caps.setCapability("tags", "Tag1");
//		    caps.setCapability("build", "v1.0");
//		    try {
//				driver = new RemoteWebDriver(new URL(URL), caps);
//			} catch (MalformedURLException e) {
//				e.printStackTrace();
//			}
//		return new SauceLabBrowsers();
//	}
//	
//	
//	public AbstractPage setUpCleanDriver(){
//		try{
//			driver.manage().window().maximize();
//			logger.info("Window has been maximized");
//			driver.manage().deleteAllCookies();
//			logger.info("All cookies are cleared and Browser is clean");
//			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//			logger.info("Implicit wait now applies");
//		}catch(Exception e){
//			logger.info("Unable to setup a clean new browser");
//		}		
//		return PageFactory.initElements(driver, AbstractPage.class);
//	}
//	
//	
//	public SauceLabBrowsers sendEmail(){
//		try {
//			logger.info("About to send emails ...");
//			SendMail mail = new SendMail();
//			mail.postMail(SendMail.emailList, SendMail.emailSubjectTxt, SendMail.emailMsgTxt, SendMail.emailFromAddress); //SendMail.emailFromAddress
//			logger.info("Email has been sent out to various managers");
//		} catch (MessagingException e) {
//			logger.error("Unable to send email out to everyone becuase of the following error message ", e);
//		}
//		return new SauceLabBrowsers();
//	}
//	
////	Cucumber ScreenCapture!!!
//	public void screenCapture(Scenario scenario) {
//        scenario.write("The Scenario name is "+scenario.getName()+" and the status is "+scenario.getStatus());
//		if (scenario.isFailed()){
//			logger.error("Scenario failed! Browser: "  + " Taking screenshot...");
//            scenario.write("Current Page URL is: " + driver.getCurrentUrl());
//            try {
//                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
//                scenario.embed(screenshot, "image/png");
//            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
//            	logger.error(somePlatformsDontSupportScreenshots.getMessage());
//            }
//		}
//	}
//	
//	public void setupDatabaseConnection(){
//		System.out.println("Starting up the Database ...");
//		try {
//			DbManager.setMysqlDbConnection();
//		} catch (ClassNotFoundException | SQLException | MessagingException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	public void closeDatabaseConnection(){
//		System.out.println("Stopping the Database ...");
//		try {
//			DbManager.shutDownDatabase();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	
//	
//	public void destroyDriver(){
//		try {
//			driver.quit();
//			logger.info("The browser has now quit");
//			driver = null;
//			logger.info("The browser has no value anymore");
//		}catch(Throwable t) {
//			logger.error("The tearDown has encountered error", t);
//		}
//	}
//	
//	
//
//	
//	
//	
//
//	
//
//
//}
