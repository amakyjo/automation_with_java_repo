package com.live.magento.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TVProductsPage extends AbstractPage{

	public TVProductsPage(WebDriver driver) {
		super(driver);
	}

	public void selectLCDTv() {
		try{
			logger.info("The method selectLCDTv started successfully");
			driver.findElement(By.id("product-collection-image-4")).click();
			logger.info("The method selectLCDTv ended successfully");
		}catch(Exception e){
			camera.takeShot("selectLCDTv");
			logger.error("The selectLCDTv encountered an error: ",e);
		}
	}
	
	
	

}
