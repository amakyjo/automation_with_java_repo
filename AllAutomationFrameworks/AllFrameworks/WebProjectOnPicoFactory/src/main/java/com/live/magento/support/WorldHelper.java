package com.live.magento.support;

import org.openqa.selenium.support.PageFactory;

import com.live.magento.browsers.Browsers;
import com.live.magento.pages.HomePage;
import com.live.magento.pages.MyAccountPage;


public class WorldHelper extends Browsers{
	
	private MyAccountPage myAccountPage;
	private HomePage homePage;

	

	       
	public HomePage getHomePage(){
		if(homePage == null){
			homePage = PageFactory.initElements(driver, HomePage.class);
		}
		return homePage;
	}
	
	public MyAccountPage getMyAccountPage(){
		if(myAccountPage == null){
			myAccountPage = PageFactory.initElements(driver, MyAccountPage.class);
		}
		return myAccountPage;
	}






}
