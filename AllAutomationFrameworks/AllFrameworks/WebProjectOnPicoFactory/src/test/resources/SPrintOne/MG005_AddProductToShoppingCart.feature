@InSprint @Regression 
Feature: Testing for Adding item to shopping Cart Functionality
  As a customer
  I want to add item(s) to shopping cart
  So I can check them out and pay for them

  @Regression
  Scenario: Test for adding Samsung LCD item to shopping cart
    Given that I am logged on with "234dotus@gmail.com"&"password123" and have searched for "LCD"
    When I add the search product "Samsung LCD" to the shooping chart
    Then the item "Samsung LCD" should exist

  Scenario: Test for adding LG LCD item to shopping cart
    Given that I am logged on with "234dotus@gmail.com"&"password123" and have searched for "LCD"
    When I add the search product "LG LCD" to the shooping chart
    Then the item "LG LCD" should exist
