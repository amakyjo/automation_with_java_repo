@InSprint @Regression 
Feature: Testing for Login functionality
  As online shopping customer
  I want to login to the application
  SO I can winter sale shopping

  Background: 
    Given that I am on the login page

  @Regression @SIT @FT
  Scenario: Testing for login functionality using valid username and valid passwword
    When I logon with username as "234dotus@gmail.com" and password as "password123"
    Then I should see "Donald Jonathan"

  Scenario: Testing for login functionality using invalid username and valid passwword
    When I logon with username as "xxxxxxxxx@gmail.com" and password as "password123"
    Then I should see "Invalid login or password"

  Scenario: Testing for login functionality using valid username and invalid passwword
    When I logon with username as "234dotus@gmail.com" and password as "xoxoxoox"
    Then I should see "Invalid login or password"

  @Regression
  Scenario: Testing for login functionality using invalid username and invalid passwword
    When I logon with username as "xxxxxxxxx@gmail.com" and password as "xoxoxoxo"
    Then I should see "Invalid login or password"

  Scenario: Testing for login functionality using no username and valid passwword
    When I logon with username as "" and password as "password123"
    Then I should see "This is a required field"

  Scenario: Testing for login functionality using valid username and no passwword
    When I logon with username as "234dotus@gmail.com" and password as ""
    Then I should see "This is a required field"

  Scenario: Testing for login functionality using no username and no passwword
    When I logon with username as "" and password as ""
    Then I should see "This is a required field"
