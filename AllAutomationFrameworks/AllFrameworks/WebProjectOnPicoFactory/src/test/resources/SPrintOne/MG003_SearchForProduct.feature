@InSprint @Regression  
Feature: Testing for search functionality
  As online shopping customer
  I want to search for product
  So I can do winter sale shopping

  @InSprint
  Scenario: Testing for advanced search functionality item name
    Given that I am on the search page
    When search for item "LG LCD"
    Then I should see item "LG LCD"

  @Regression
  Scenario: Testing for advanced search functionality item name
    Given that I am logged on with "234dotus@gmail.com" & "password123" and landed on the search page
    When search for item "LG LCD"
    Then I should see item "LG LCD"
