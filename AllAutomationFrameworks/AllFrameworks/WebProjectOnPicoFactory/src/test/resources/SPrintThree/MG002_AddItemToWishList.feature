@InSprint @UAT @Regression @Troubleshooting
Feature: Testing Adding to Wishlist Functionality
  As a customer who have not made up his mind
  I want to browse for item that I like add it to the wish list
  So I can purchase them have madea decision on what I reaaly want

  Scenario Outline: Add one item to the Wishlist Cart
    Given I am on the search result page
      | username           | password    | itemName |
      | 234dotus@gmail.com | password123 | LCD      |
    When I add "<itemName>" to the wishlist
    Then the I should see "<itemModel>" and "<itemAmount>" on the wishlist

    Examples: This the data for the test above
      | itemName    | itemModel | itemAmount |
      | Samsung LCD | SKU TV002 | $700.00    |
      | LG LCD      | SKU TV001 | $615.00    |
