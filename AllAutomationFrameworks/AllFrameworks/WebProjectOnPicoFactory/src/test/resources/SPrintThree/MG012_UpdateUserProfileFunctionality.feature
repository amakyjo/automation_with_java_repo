Feature: Testing for Data intergrity on the Database
  As a super user/admin in Magento plc
  I want to update user account at the front-end 
  So I can validate the data from the back-end

  @Regression @SIT @Database @wip
  Scenario Outline: Testing for Data Consistency on the Database
    Given that I am on My Account Page
      | Username           | Password    |
      | 234dotus@gmail.com | password123 |
    When I update an account with :"<First Name>", "<Last Name>"&"<Email Address>",
    Then the details should exist at the database:"<First Name>" & "<Last Name>"

    Examples: Data for Account Creation
      | First Name | Last Name | Email Address      | First Name | Last Name |
      | John       | Wilson    | 234dotus@gmail.com | John       | Wilson    |
      | Donald     | Jonathan  | 234dotus@gmail.com | Donald     | Jonathan  |
