@InSprint @Regression  @SIT 
Feature: Testing wishlist Functionality
  As a customer who have not made up his mind
  I want to browse for item that I can add and delete it to the wish list
  So I can select a different product

  @Regression
  Scenario Outline: Remove one item from the Wishlist Cart
    Given I am on the wishlist page
      | username           | password    | itemCategory | itemName    |
      | 234dotus@gmail.com | password123 | LCD          | Samsung LCD |
    When I delete "<itemName>" from the wishlist
    Then the "<itemModel>" and "<itemAmount>" should no longer exist on the wishlist

    Examples: This the data for the test above
      | itemName    | itemModel | itemAmount |
      | Samsung LCD | SKU TV002 | $700.00    |

  @Regression
  Scenario Outline: Remove one item from the Wishlist Cart
    Given I am on the wishlist page
      | username           | password    | itemCategory | itemName |
      | 234dotus@gmail.com | password123 | LCD          | LG LCD   |
    When I delete "<itemName>" from the wishlist
    Then the "<itemModel>" and "<itemAmount>" should no longer exist on the wishlist

    Examples: This the data for the test above
      | itemName | itemModel | itemAmount |
      | LG LCD   | SKU TV001 | $615.00    |

  @Regression
  Scenario Outline: Attempt to remove one item from the Wishlist Cart but later change mind
    Given I am on the wishlist page
      | username           | password    | itemCategory | itemName    |
      | 234dotus@gmail.com | password123 | LCD          | Samsung LCD |
    When I try to delete "<itemName>" from the wishlist but change my mind
    Then the "<itemModel>" and "<itemAmount>" should exist on the wishlist

    Examples: This the data for the test above
      | itemName    | itemModel | itemAmount |
      | Samsung LCD | SKU TV002 | $700.00    |

  @Regression
  Scenario Outline: Attempt to remove one item from the Wishlist Cart but later change mind
    Given I am on the wishlist page
      | username           | password    | itemCategory | itemName |
      | 234dotus@gmail.com | password123 | LCD          | LG LCD   |
    When I try to delete "<itemName>" from the wishlist but change my mind
    Then the "<itemModel>" and "<itemAmount>" should exist on the wishlist

    Examples: This the data for the test above
      | itemName | itemModel | itemAmount |
      | LG LCD   | SKU TV001 | $615.00    |
