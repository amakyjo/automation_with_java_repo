@All
Feature: Testing Advanced Search Functionality
  As online shopping customer
  I want to use the advanced search functionality
  So I can search for products to purchase

  @Regression
  Scenario Outline: Test Advanced Search on Magento for product
    Given that I am on the home page
    When I submit the advanced search form with "<Name>", "<Description>", "<Short Description>","<SKU>", "<Price>","<Price To>" and "<Tax Class>"
    Then "<Item Name>", "<Item Price>" and "<Message>" should exist on the contact us page

    Examples: 
      | Name   | Description             | Short Description | SKU   | Price | Price To | Tax Class     | Item Name | Item Price                                              | Message                 |
      | LG LCD | LG LCD is a good option | TV                | TV001 | 500   | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      |        | LG LCD is a good option | TV                | TV001 | 500   | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD |                         | TV                | TV001 | 500   | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   | TV001 | 500   | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                |       | 500   | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 |       | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 | 500   |          | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 |       |          | None          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      |        |                         | TV                | TV001 | 500   | 1000     | none          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   |       | 500   | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   |       | 500   | 1000     | NONE          | LG LCD    | $615.00                                                 | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 | 500   | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      |        | LG LCD is a good option | TV                | TV001 | 500   | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD |                         | TV                | TV001 | 500   | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   | TV001 | 500   | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                |       | 500   | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 |       | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 | 500   |          | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 |       |          | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      |        |                         | TV                | TV001 | 500   | 1000     | Taxable Goods |           | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   |       | 500   | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   |       | 500   | 1000     | Taxable Goods | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 | 500   | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      |        | LG LCD is a good option | TV                | TV001 | 500   | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD |                         | TV                | TV001 | 500   | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   | TV001 | 500   | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                |       | 500   | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 |       | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 | 500   |          | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option | TV                | TV001 |       |          | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      |        |                         | TV                | TV001 | 500   | 1000     | Shipping      |           | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   |       | 500   | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
      | LG LCD | LG LCD is a good option |                   |       | 500   | 1000     | Shipping      | LG LCD    | No items were found using the following search criteria | Catalog Advanced Search |
