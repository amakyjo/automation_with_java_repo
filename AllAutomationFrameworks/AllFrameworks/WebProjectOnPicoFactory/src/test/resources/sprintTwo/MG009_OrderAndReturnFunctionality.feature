@InSprint @Regression 
Feature: Testing Order and Return Functionality
  As online shopping customer
  I want to track my order and return status
  So I can know it would be delivered

  Scenario Outline: Test Order and Return for pending deliveries
    Given that I am on the home page
    When I track my order with the these data:"<Oder ID>","<Billing Last Name>","<Find Order By>"&"<Email Address>"
    Then the following should exist: "<Status>","<Message>"&"<Grand Total>"

    Examples: Various scenarios and data
      | Oder ID   | Billing Last Name | Find Order By | Email Address      | Status                                                             | Grand Total | Message           |
      | 100000976 | Jonathan          | Email Address | 234dotus@gmail.com | Pending                                                            | $7,525.00   | Order Information |
      | 100000974 | Jonathan          | Email Address | 234dotus@gmail.com | Pending                                                            | $1,125.00   | Order Information |
      | 100000973 | Jonathan          | Email Address | 234dotus@gmail.com | Pending                                                            | $620.00     | Order Information |
      | 100001044 | Jonathan          | Email Address | 234dotus@gmail.com | Pending                                                            | $620.00     | Order Information |
      | 100001044 | Jonathan          | Email Address | 234dotus@gmail.com | Pending                                                            | $620.00     | Order Information |
      | 100000974 | Jonathan          | ZIP Code      | e13 0qe            | Pending                                                            | $1,125.00   | Order Information |
      |           | Jonathan          | ZIP Code      | e13 0qe            | This is a required field                                           |             | Required Fields   |
      | 100000974 | Jose Fernandes    | Email Address | 234dotus@gmail.com | Entered data is incorrect. Please try again                        |             | Required Fields   |
      | 100000973 | Jonathan          | ZIP Code      | e13 0qe            | Pending                                                            | $620.00     | Order Information |
      |           | Jose Fernandes    | ZIP Code      | e13 0qe            | This is a required field                                           |             | Required Fields   |
      | 100000976 | Jonathan          | Email Address |                    | This is a required field                                           |             | Required Fields   |
      | 100000976 | Jonathan          | ZIP Code      |                    | This is a required field                                           |             | Required Fields   |
      | 100000976 |                   | Email Address | 234dotus@gmail.com | This is a required field                                           |             | Required Fields   |
      | 100000976 |                   | ZIP Code      | e13 0qe            | This is a required field                                           |             | Required Fields   |
      | 100000976 | Jonathan          | Email Address | e13 0qe            | Please enter a valid email address. For example johndoe@domain.com |             | Required Fields   |
      | 100000976 | Jonathan          | ZIP Code      | 234dotus@gmail.com | Entered data is incorrect. Please try again                        |             | Required Fields   |
