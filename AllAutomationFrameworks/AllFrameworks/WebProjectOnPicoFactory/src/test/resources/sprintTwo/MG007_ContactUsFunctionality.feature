@InSprint @Regression 
Feature: Testing Contact Us Functionality
  As online shopping customer
  I want to contact Magento Plc
  So I can inform them about my missing product

  @Sanity @sample 
  Scenario Outline: Test Contact US on Magento for missing product
    Given that I am on the home page
    When I submit the contact us form with "<Name>", "<Email Address>", "<Telephone>" and "<Comment>"
    Then "<Success Message>" and "<Page Title>" should exist on the contact us page

    Examples: Various data for the outlone above
      | Name     | Email Address       | Telephone     | Comment                                        | Success Message                                                                                      | Page Title |
      | Don Moen | don.mone@living.com | 0333 003 0100 | Kindly check your record as my item is missing | Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us | Contact Us |
      |          | don.mone@living.com | 0333 003 0103 | Kindly check your record as my item is missing | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone.don.mone   | 0333 003 0102 | Kindly check your record as my item is missing | Required Fields                                                                                      | Contact Us |
      | Don Moen |                     | 0333 003 0101 | Kindly check your record as my item is missing | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com |               | Kindly check your record as my item is missing | Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us | Contact Us |
      | Don Moen | don.mone@living.com | 0333 003 0101 |                                                | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com |               |                                                | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com | 0333 003 0100 | Kindly check your record as my item is missing | Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us | Contact Us |
      |          | don.mone@living.com | 0333 003 0103 | Kindly check your record as my item is missing | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone.don.mone   | 0333 003 0102 | Kindly check your record as my item is missing | Required Fields                                                                                      | Contact Us |
      | Don Moen |                     | 0333 003 0101 | Kindly check your record as my item is missing | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com |               | Kindly check your record as my item is missing | Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us | Contact Us |
      | Don Moen | don.mone@living.com | 0333 003 0101 |                                                | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com |               |                                                | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com | 0333 003 0100 | Kindly check your record as my item is missing | Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us | Contact Us |
      |          | don.mone@living.com | 0333 003 0103 | Kindly check your record as my item is missing | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone.don.mone   | 0333 003 0102 | Kindly check your record as my item is missing | Required Fields                                                                                      | Contact Us |
      | Don Moen |                     | 0333 003 0101 | Kindly check your record as my item is missing | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com |               | Kindly check your record as my item is missing | Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us | Contact Us |
      | Don Moen | don.mone@living.com | 0333 003 0101 |                                                | This is a required field                                                                             | Contact Us |
      | Don Moen | don.mone@living.com |               |                                                | This is a required field                                                                             | Contact Us |
