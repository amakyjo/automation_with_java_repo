@InSprint @Regression 
Feature: Validate Customer Service Functionality in the web application
  As a user
  I would like to view the customer services
  So I can access the services

  Scenario Outline: Validate the customer services in the web application
    Given I am on the customer service page
    Then I should see "<Page Url>","<Page Title>","<Message One>","<Message Two>"

    Examples: Data For various scenario
      | Page Url                                          | Page Title       | Message One      | Message Two      |
      | http://live.guru99.com/index.php/customer-service | Customer Service | Customer Service | Compare Products |
      | http://live.guru99.com/index.php/customer-service | Customer Service | Customer Service | Compare Products |
      | http://live.guru99.com/index.php/customer-service | Customer Service | Customer Service | Compare Products |
      | http://live.guru99.com/index.php/customer-service | Customer Service | Customer Service | Compare Products |
      | http://live.guru99.com/index.php/customer-service | Customer Service | Customer Service | Compare Products |
      | http://live.guru99.com/index.php/customer-service | Customer Service | Customer Service | Compare Products |
