@InSprint @Regression
Feature: Testing for Data intergrity on the Database(MySQL)
  As a super user/admin in Magento plc
  I want to create account at the front-end 
  So I can validate the data from the back-end

  @Regression @SIT @Database @Joy
  Scenario Outline: Testing for Data Conistency on the Database
    Given that I am on the home page
    When I create an account with :"<First Name>", "<Last Name>","<Email Address>","<Password>" &"<Confirm Password>",
    Then the account details should exist at the database:"<First Name>"
    And "<Success Message>" should exist on the front-end

    Examples: Data for Account Creation
      | First Name | Last Name | Email Address         | Password    | Confirm Password | Success Message                                   |
      | Jack       | Will      | jack.will@yahoo.com   | password456 | password456      | Thank you for registering with Main Website Store |
      | Ella       | Robert    | ella.robert@yahoo.com | password456 | password456      | Thank you for registering with Main Website Store |
      | Ego        | Nwofor    | ego.robert@yahoo.com  | password456 | password456      | Thank you for registering with Main Website Store |
      | Jane       | Walker    | jane.walker@yahoo.com | password456 | password456      | Thank you for registering with Main Website Store |
