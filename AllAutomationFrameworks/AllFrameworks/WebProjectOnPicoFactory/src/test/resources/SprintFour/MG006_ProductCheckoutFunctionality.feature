@InSprint @Pending 
Feature: Testing the checkout functionality of the application
  As online shopping customer
  I want to add item to shopping cart and checkout
  So I can have them delivered to an address

  Scenario Outline: Checkout various different Combinations with different information
    Given that I am authenticated and have searched for "<Item To Be Searched>"
      | Username           | Password    |
      | 234dotus@gmail.com | password123 |
    When I add "<Item To Be Searched>" to cart and checkout with:"<Country>", "<Province/State>","<Zip Code>", "<Billing address>","<Shipping address>" &"<Type of Payment>"
    Then I should see the following information: "<Success Message1>" & "<Success Message2>"

    Examples: This is the needed extra information
      | Item To Be Searched | Country        | Province/State | Zip Code | Billing address                  | Shipping address | Type of Payment   | Success message1             | Success message2             |
      | LG LCD              | United States  | California     | 90210    | Default Address                  | Default Address  | Check/Money order | Thank you for your purchase! | Your order has been received |
      | LG LCD              | United Kingdom | Epsom          | EE54 2BT | 88 Dolby Square,Sulfoks, England | Default Address  | Check/Money order | Thank you for your purchase! | Your order has been received |
