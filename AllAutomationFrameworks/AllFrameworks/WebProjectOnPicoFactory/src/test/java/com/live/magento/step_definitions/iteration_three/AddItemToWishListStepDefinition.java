package com.live.magento.step_definitions.iteration_three;

import java.util.List;

import org.junit.Assert;

import com.live.magento.pages.MyWishListPage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddItemToWishListStepDefinition {
	
	
	private WorldHelper helper;
	private SearchResultPage searchResultPage;
	private MyWishListPage myWishListPage;
	private String productName;

	public AddItemToWishListStepDefinition(WorldHelper helper){
		this.helper = helper;
	}

	
	
	@Given("^I am on the search result page$")
	public void i_am_on_the_search_result_page(DataTable loginAndItemNameData) throws Throwable {
		List<List<String>> processedData = loginAndItemNameData.raw();
		String username = processedData.get(1).get(0);
		String password = processedData.get(1).get(1);
		String productName = processedData.get(1).get(2);
		searchResultPage = helper
				.getHomePage()
				.loadDefaultPage()
				.openMyAccount()
				.loginWith(username, password)
				.visitHomePage()
				.searchForProduct(productName);
	}

	@When("^I add \"([^\"]*)\" to the wishlist$")
	public void i_add_to_the_wishlist(String productName) throws Throwable {
		this.productName = productName;
		myWishListPage = searchResultPage
				.selectItemToBeAddedToCart(productName)
				.addToWishList(productName); 
	}

	@Then("^the I should see \"([^\"]*)\" and \"([^\"]*)\" on the wishlist$")
	public void the_I_should_see_and_on_the_wishlist(String itemModel, String itemAmount) throws Throwable {
		try{
			Assert.assertTrue(itemModel+"  does not exist", myWishListPage.validateItemModel(itemModel));
			Assert.assertTrue(itemAmount+ " does not exist", myWishListPage.validateItemAmount(itemAmount));
		}finally {
//			Tearing down Test Data
			myWishListPage.deleteProductFromWishListWithoutChangingMyMind(productName);	
		}
	}
	

	
}
