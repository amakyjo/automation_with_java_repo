package com.live.magento.step_definitions.iteration_two;

import org.junit.Assert;

import com.live.magento.pages.ContactUsPage;
import com.live.magento.pages.HomePage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ContactUsFeatureStepDef {
	
	private WorldHelper helper;
	private ContactUsPage contactUsPage;
	private HomePage homePage;

	public ContactUsFeatureStepDef(WorldHelper helper){
		this.helper = helper;
	}
	
	@Given("^that I am on the home page$")
	public void thatIAmOnTheHomePage() throws Throwable {
	    homePage = helper
	    		.getHomePage()
	    		.loadDefaultPage();
	}
	
	@When("^I submit the contact us form with \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void iSubmitTheContactUsFormWithAnd(String name, String email, String phone, String comment) throws Throwable {
		contactUsPage = homePage
				.openContactUsForm()
				.fillAndSubmitContactUsFormWithTheFollowingDetails(name, email, phone, comment);
	}

	@Then("^\"([^\"]*)\" and \"([^\"]*)\" should exist on the contact us page$")
	public void andShouldExistOnTheContactUsPage(String successMessage, String pageTitle) throws Throwable {
		Assert.assertTrue("The "+successMessage+ " does not exist", contactUsPage.validateSuccessMessage(successMessage));
		Assert.assertTrue("The "+pageTitle+ " does not exist", contactUsPage.validatePageTitle(pageTitle));
	}
	
	
}
