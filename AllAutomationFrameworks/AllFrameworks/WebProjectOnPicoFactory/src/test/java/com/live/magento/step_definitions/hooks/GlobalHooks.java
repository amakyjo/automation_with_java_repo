package com.live.magento.step_definitions.hooks;



import com.live.magento.browsers.Browsers;

import cucumber.api.java.Before;

public class GlobalHooks {
	
	

	private static boolean dunit = false;
	private static Browsers browsers;

	public GlobalHooks(Browsers browsers){
		this.browsers = browsers;
	}

	
    
    

    @Before(order=1)
    public static void beforeAll()  {
        if(!dunit == true) {
        	
//            Code to run once at the beginning of the entire Test
            	try {
                	System.out.println("Run once at start time.............................................");
                	browsers.setupDatabaseConnection();
            	}catch(Throwable t) {
            		t.printStackTrace();
            	}
    		
            
            
            Runtime.getRuntime().addShutdownHook(new Thread(){
            	public void run(){
//            		Codes to run at the end of the entire Test
            		try {
                		browsers.sendEmail();
                		browsers.closeDatabaseConnection();
                		System.out.println("Run Once all Tests are Done");
                	}catch(Throwable t) {
                		t.printStackTrace();
                	}
                }} );

            dunit = true;
        }
    }


}
