package com.live.magento.step_definitions.iteration_one;

import org.junit.Assert;

import com.live.magento.pages.SearchResultPage;
import com.live.magento.pages.ShoppingCartPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AddToCartFeatureStepDefinition {
	
	private WorldHelper helper;
	private SearchResultPage searchResultPage;
	private ShoppingCartPage shoppingCartPage;

	public AddToCartFeatureStepDefinition(WorldHelper helper){
		this.helper = helper;
	}

	@Given("^that I am logged on with \"([^\"]*)\"&\"([^\"]*)\" and have searched for \"([^\"]*)\"$")
	public void that_I_am_logged_on_with_and_have_searched_for(String username, String password, String productName) throws Throwable {
		searchResultPage = helper
		.getHomePage()
		.loadDefaultPage()
		.openMyAccount()
		.loginWith(username, password)
		.visitHomePage()
		.searchForProduct(productName);
	}

	@When("^I add the search product \"([^\"]*)\" to the shooping chart$")
	public void i_add_the_search_product_to_the_shooping_chart(String productName) throws Throwable {
		shoppingCartPage = searchResultPage
				.selectItemToBeAddedToCart(productName)
				.addToCart(productName);
	}

	@Then("^the item \"([^\"]*)\" should exist$")
	public void the_item_should_exist(String productName) throws Throwable {
		try {
		Assert.assertTrue(productName+" does not exist", shoppingCartPage.validateItem(productName));
//		Tearing Down Test Data
		}finally {
			shoppingCartPage.deleteProductFromShoppingCartWithoutChangingMyMind(productName);
		}
		
	}
	


}
