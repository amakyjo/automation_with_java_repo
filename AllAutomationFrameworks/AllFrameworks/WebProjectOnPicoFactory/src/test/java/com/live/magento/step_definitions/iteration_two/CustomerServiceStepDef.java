package com.live.magento.step_definitions.iteration_two;

import org.junit.Assert;

import com.live.magento.pages.CustomerServicePage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class CustomerServiceStepDef {
	
	private WorldHelper helper;
	private CustomerServicePage customerServicePage;

	public CustomerServiceStepDef(WorldHelper helper) {
		this.helper = helper;
	}
	
	@Given("^I am on the customer service page$")
	public void iAmOnTheCustomerServicePage() throws Throwable {
		customerServicePage = 
				helper
				.getHomePage()
				.loadDefaultPage()
				.goToCustomerService();
	}

	@Then("^I should see \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void iShouldSee(String pageUrl, String pageTitle, String messageOne, String messageTwo) throws Throwable {
		Assert.assertTrue("Does not contain "+pageUrl, customerServicePage.validateUrl(pageUrl));
		Assert.assertTrue("Does not contain "+pageTitle, customerServicePage.validatePageTitle(pageTitle));
		Assert.assertTrue("Does not contain "+messageOne, customerServicePage.validateMessage(messageOne));
		Assert.assertTrue("Does not contain "+messageTwo, customerServicePage.validateMessage(messageTwo));
	}

}
