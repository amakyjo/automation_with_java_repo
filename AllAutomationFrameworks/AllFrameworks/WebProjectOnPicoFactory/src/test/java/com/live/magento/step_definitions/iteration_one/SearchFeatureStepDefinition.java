package com.live.magento.step_definitions.iteration_one;

import com.live.magento.pages.HomePage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchFeatureStepDefinition {
	
	private WorldHelper helper;
	private HomePage homePage;
	private SearchResultPage searchResultPage;

	public SearchFeatureStepDefinition(WorldHelper helper){
		this.helper = helper;
	}
	

	
	@Given("^that I am on the search page$")
	public void that_I_am_search_page() throws Throwable {
		homePage = helper
				.getHomePage()
				.loadDefaultPage();
	}

	@When("^search for item \"([^\"]*)\"$")
	public void search_for_item(String productName) throws Throwable {
		searchResultPage = homePage.searchForProduct(productName);
	}

	@Then("^I should see item \"([^\"]*)\"$")
	public void i_should_see_item(String productName) throws Throwable {
		searchResultPage.verifyTheProduct(productName);
	}
	
	
	@Given("^that I am logged on with \"([^\"]*)\" & \"([^\"]*)\" and landed on the search page$")
	public void that_I_am_logged_on_with_and_landed_on_the_search_page(String username, String password) throws Throwable {
		homePage = helper
				.getHomePage()
				.loadDefaultPage()
				.openMyAccount()
				.loginWith(username, password)
				.visitHomePage();
	}

}
