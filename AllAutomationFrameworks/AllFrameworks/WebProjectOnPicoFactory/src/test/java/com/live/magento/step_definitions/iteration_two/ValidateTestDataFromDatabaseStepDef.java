package com.live.magento.step_definitions.iteration_two;

import org.junit.Assert;

import com.live.magento.pages.MyDashaboardPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class ValidateTestDataFromDatabaseStepDef {
	
	private WorldHelper helper;
	private MyDashaboardPage myDashboardPage;


	public ValidateTestDataFromDatabaseStepDef(WorldHelper helper) {
		this.helper = helper;
	}
	
	@When("^I create an account with :\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" &\"([^\"]*)\",$")
	public void iCreateAnAccountWith(String firstName, String lastName, String emailAddress, String password, String confirmPassword) throws Throwable {
	   myDashboardPage = 
			   	helper
			   .getHomePage()
			   .openMyAccount()
			   .createAnAccount()
			   .completeTheAccountFormAndSubmit(firstName, lastName, emailAddress, password, confirmPassword);
	}

	@Then("^the account details should exist at the database:\"([^\"]*)\"$")
	public void theAccountDetailsShouldExistAtTheDatabaseAnd(String username) throws Throwable {
	    Assert.assertTrue("This "+username+" does not exist on the Database", myDashboardPage.getDatabaseQueryResult(username));
	}

	@Then("^\"([^\"]*)\" should exist on the front-end$")
	public void should_exist_on_the_front_end(String successMessage) throws Throwable {
		Assert.assertTrue("This "+successMessage+" does not exist on the Database", myDashboardPage.getSubmissionFeedback(successMessage));
	}

}
