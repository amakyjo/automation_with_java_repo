package com.live.magento.step_definitions.iteration_four;

import java.util.List;

import com.live.magento.pages.OrderSummaryPage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductCheckoutStepDefinition {
	private WorldHelper helper ;
	private SearchResultPage searchResultPage;
	private OrderSummaryPage orderSummaryPage;
	
	
	public ProductCheckoutStepDefinition(WorldHelper helper){
		this.helper = helper;
	}
	
	
	
	@Given("^that I am authenticated and have searched for \"([^\"]*)\"$")
	public void that_I_am_authenticated_and_have_searched_for(String productName, DataTable loginData) throws Throwable {
		List<List<String>> processedLoginData = loginData.raw();
		String username = processedLoginData.get(1).get(0);
		String password = processedLoginData.get(1).get(1);
		System.out.println(helper);
		searchResultPage = helper
				.getHomePage()
				.loadDefaultPage()
				.openMyAccount()
				.loginWith(username, password)
				.visitHomePage()
				.searchForProduct(productName);
	}

	@When("^I add \"([^\"]*)\" to cart and checkout with:\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\" &\"([^\"]*)\"$")
	public void i_add_to_cart_and_checkout_with(String productName, String country, String province, String zipCode, String billingAddress, String shippingAddress, String paymentType) throws Throwable {
		orderSummaryPage = searchResultPage
				.selectItemToBeAddedToCart(productName)
				.addToCart(productName)
				.proceedToCheckout(country, province, zipCode)
				.completeBillingInformation(billingAddress)
				.completeShippingInformation(billingAddress)
				.completeShippingMethod()
				.completePaymentInformation(paymentType)
				.placeOrder();
	}

	@Then("^I should see the following information: \"([^\"]*)\" & \"([^\"]*)\"$")
	public void i_should_see_the_following_information(String successMessage1, String successMessage2) throws Throwable {
		orderSummaryPage.validateTheFollowing(successMessage1, successMessage2);
	}

}
