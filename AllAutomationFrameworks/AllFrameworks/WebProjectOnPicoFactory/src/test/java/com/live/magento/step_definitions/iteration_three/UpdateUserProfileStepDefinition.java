package com.live.magento.step_definitions.iteration_three;

import java.util.List;

import org.junit.Assert;

import com.live.magento.pages.MyDashaboardPage;
import com.live.magento.pages.WelcomePage;
import com.live.magento.support.WorldHelper;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class UpdateUserProfileStepDefinition {
	private WorldHelper helper;
	private WelcomePage welcomePage;
	private MyDashaboardPage myDashaboardPage;
	private String username;
	private String password;

	public UpdateUserProfileStepDefinition(WorldHelper helper) {
		this.helper = helper;
	}

	@Given("^that I am on My Account Page$")
	public void thatIAmOnMyAccountPage(DataTable table) throws Throwable {
		List<List<String>> dataTable = table.raw();
		this.username = dataTable.get(1).get(0);
		this.password = dataTable.get(1).get(1);
	    welcomePage = helper.getHomePage()
	    		.loadDefaultPage()
	    		.openMyAccount()
	    		.loginWith(username, password);
	}
	
	@When("^I update an account with :\"([^\"]*)\", \"([^\"]*)\"&\"([^\"]*)\",$")
	public void iUpdateAnAccountWith(String firstName, String lastName, String emailAddress) throws Throwable {
		myDashaboardPage = welcomePage
				.goToAccountInformation()
				.editAccountWith(firstName, lastName, emailAddress)
				.saveNewDetails();
	}
	
	@Then("^the details should exist at the database:\"([^\"]*)\" & \"([^\"]*)\"$")
	public void theDetailsShouldExistAtTheDatabase(String firstName1, String lastName1) throws Throwable {
		try{
			Assert.assertFalse("Does not contain "+firstName1, !myDashaboardPage.getDatabaseQueryForProfileUpdateFirstName(firstName1));
			Assert.assertFalse("Does not contain "+lastName1,  !myDashaboardPage.getDatabaseQueryForProfileUpdateLastName(lastName1));
		}finally {
//			helper
//			.getHomePage().loadDefaultPage()
//			.openMyAccount().loginWith(username, password)
//			.goToAccountInformation()
//			.editAccountWith("Donald", "Jonathan", "234dotus@gmail.com")
//			.saveNewDetails();
		}
		
	}


}
