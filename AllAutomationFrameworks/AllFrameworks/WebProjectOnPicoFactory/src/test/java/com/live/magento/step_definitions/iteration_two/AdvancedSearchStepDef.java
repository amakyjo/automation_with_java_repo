package com.live.magento.step_definitions.iteration_two;

import org.junit.Assert;

import com.live.magento.pages.SearchResultsPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AdvancedSearchStepDef {
	
	private WorldHelper helper ;
	private SearchResultsPage searchResultPage;
	

	public AdvancedSearchStepDef(WorldHelper helper) {
		this.helper = helper;
	}
	
	
	@When("^I submit the advanced search form with \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_submit_the_advanced_search_form_with_and(String name, String description, String shortDescription, String sKU, String price, String priceTo, String taxClass) throws Throwable {
		searchResultPage =  
				helper
				.getHomePage()
				.clickOnAdvancedSearch()
				.completeAdvancedSearchForm(name, description, shortDescription, sKU, price, priceTo, taxClass);
	}
	
	@Then("^\"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" should exist on the contact us page$")
	public void and_should_exist_on_the_contact_us_page(String itemName, String itemPrice, String message) throws Throwable {
	    Assert.assertTrue(itemName+" does not exist", searchResultPage.validateProductInfo(itemName));
	    Assert.assertTrue(itemPrice+" does not exist", searchResultPage.validateProductInfo(itemPrice));
	    Assert.assertTrue(message+" does not exist", searchResultPage.validateProductInfo(message));
	}
	

}
