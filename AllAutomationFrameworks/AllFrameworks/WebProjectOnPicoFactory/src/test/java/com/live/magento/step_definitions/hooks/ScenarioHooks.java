package com.live.magento.step_definitions.hooks;




import com.live.magento.browsers.Browsers;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class ScenarioHooks {
	
	
	private Browsers browsers;

	public ScenarioHooks(Browsers browsers){
		this.browsers = browsers;
	}
	
	
	@Before(order=2)
	public void setUpDownTestEnvironment(){
		browsers.instatiateDriver().setUpCleanDriver(); 
	}
	

	
	
	@After()
	public void tearDownTestEnvironment(Scenario scenario){		
		browsers.screenCapture(scenario);		
		browsers.destroyDriver();
	}
	


}
