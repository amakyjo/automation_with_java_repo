package com.live.magento.step_definitions.hooks;

import com.live.magento.browsers.Browsers;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class TaggedHooks {
	
	private Browsers browsers;


	public TaggedHooks(Browsers browsers) {
		this.browsers = browsers;
	}
	
	
	@Before("@in-progress")
	public void startTest() {
		System.out.println("Work In Progress Hooks fired!!!");
	}
	
	
	@After("~@in-progress")
	public void tearDownTestEnvironment(Scenario scenario){
		System.out.println("Work In Progress Hooks ended clean!!!");
	}

}
