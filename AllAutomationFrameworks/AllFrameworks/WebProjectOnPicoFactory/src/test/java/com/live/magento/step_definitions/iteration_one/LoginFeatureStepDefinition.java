package com.live.magento.step_definitions.iteration_one;


import com.live.magento.pages.MyAccountPage;
import com.live.magento.pages.WelcomePage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginFeatureStepDefinition {
	
	private WorldHelper helper;
	private MyAccountPage myAccountPage;
	private WelcomePage welcomePage;
	
	

	public LoginFeatureStepDefinition(WorldHelper helper){
		this.helper = helper;
	}

	
	
	
	@Given("^that I am on the login page$")
	public void that_I_am_on_the_home_page() throws Throwable {
		myAccountPage = helper
				.getHomePage()
				.loadDefaultPage()
				.openMyAccount();
	}

	@When("^I logon with username as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void i_logon_with_username_as_and_password_as(String username, String password) throws Throwable {
		welcomePage = myAccountPage.loginWith(username, password);
	}


	@Then("^I should see \"([^\"]*)\"$")
	public void i_should_see(String message) throws Throwable {
		welcomePage.verifyLogin();
	}


	


}
