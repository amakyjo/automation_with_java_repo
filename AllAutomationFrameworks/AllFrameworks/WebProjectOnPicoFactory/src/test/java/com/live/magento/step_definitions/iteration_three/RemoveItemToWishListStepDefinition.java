package com.live.magento.step_definitions.iteration_three;

import java.util.List;

import org.junit.Assert;

import com.live.magento.pages.MyWishListPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RemoveItemToWishListStepDefinition {
	
	private WorldHelper helper;
	private MyWishListPage myWishListPage;
	private String productName;

	public RemoveItemToWishListStepDefinition(WorldHelper helper){
		this.helper = helper;
	}
	
	
	
	
	@Given("^I am on the wishlist page$")
	public void i_am_on_the_wishlist_page(DataTable loginAndItemNameData) throws Throwable {
		List<List<String>> processedData = loginAndItemNameData.raw();
		String username = processedData.get(1).get(0);
		String password = processedData.get(1).get(1);
		String itemCategory = processedData.get(1).get(2);
		String productName = processedData.get(1).get(3);
		myWishListPage = helper
				.getHomePage()
				.loadDefaultPage()
				.openMyAccount()
				.loginWith(username, password)
				.visitHomePage()
				.searchForProduct(itemCategory)
				.selectItemToBeAddedToCart(productName)
				.addToWishList(productName);
	}

	@When("^I delete \"([^\"]*)\" from the wishlist$")
	public void i_delete_to_the_wishlist(String productName) throws Throwable {
	    myWishListPage.deleteProductFromWishListWithoutChangingMyMind(productName);
	}

	@Then("^the \"([^\"]*)\" and \"([^\"]*)\" should no longer exist on the wishlist$")
	public void the_and_should_no_longer_exist_on_the_wishlist(String itemModel, String itemAmount) throws Throwable {
		Assert.assertTrue(itemModel+"  does exist", !myWishListPage.validateItemModel(itemModel));
		Assert.assertTrue(itemAmount+ " does exist", !myWishListPage.validateItemAmount(itemAmount));
		myWishListPage.validateTheFollowingInTheWishListCartDoesNotExist(itemModel, itemAmount);
	}

	
	@When("^I try to delete \"([^\"]*)\" from the wishlist but change my mind$")
	public void i_try_to_delete_from_the_wishlist_but_change_my_mind(String productName) throws Throwable {
		this.productName = productName;
		myWishListPage.attemptToDeleteItemFromWishListAndLaterChangeMyMind(productName);
	}

	@Then("^the \"([^\"]*)\" and \"([^\"]*)\" should exist on the wishlist$")
	public void the_and_should_exist_on_the_wishlist(String itemModel, String itemAmount) throws Throwable {
		try {
			Assert.assertTrue(itemModel+"  does not exist", myWishListPage.validateItemModel(itemModel));
			Assert.assertTrue(itemAmount+ " does not exist", myWishListPage.validateItemAmount(itemAmount));
		}finally {
//			Tearing Down Test Data 
			myWishListPage.deleteProductFromWishListWithoutChangingMyMind(productName);
		}
	}
}
