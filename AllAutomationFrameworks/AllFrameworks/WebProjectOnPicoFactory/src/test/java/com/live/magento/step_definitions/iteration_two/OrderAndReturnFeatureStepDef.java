package com.live.magento.step_definitions.iteration_two;

import org.junit.Assert;

import com.live.magento.pages.OrderStatusPage;
import com.live.magento.support.WorldHelper;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class OrderAndReturnFeatureStepDef {
	
	private WorldHelper helper;
	private OrderStatusPage orderStatusPage;

	public OrderAndReturnFeatureStepDef(WorldHelper helper) {
		this.helper = helper;
	}
	
	@When("^I track my order with the these data:\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"&\"([^\"]*)\"$")
	public void iTrackMyOrderWithTheTheseData(String orderID, String billingLastName, String findOrderBy, String emailAddress) throws Throwable {
	    orderStatusPage = 
	    		 helper
	    		.getHomePage()
	    		.goToOrderAAndReturn()
	    		.completeForm(orderID, billingLastName, findOrderBy, emailAddress)
	    		.trackOrder();
	}

	@Then("^the following should exist: \"([^\"]*)\",\"([^\"]*)\"&\"([^\"]*)\"$")
	public void theFollowingShouldExist(String status, String message, String grandTotal) throws Throwable {
		Assert.assertTrue("Does not contain "+status, orderStatusPage.validateResult(status));
		Assert.assertTrue("Does not contain "+message, orderStatusPage.validateResult(message));
		Assert.assertTrue("Does not contain "+grandTotal, orderStatusPage.validateResult(grandTotal));
	}

}
