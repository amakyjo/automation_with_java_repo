package com.live.magento.test_area;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.live.magento.pages.AbstractPage;
import com.live.magento.pages.HomePage;
import com.live.magento.pages.MyAccountPage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.pages.WelcomePage;
import com.live.magento.utilities.Constant;
import com.live.magento.utilities.ExcelReader;
import com.live.magento.utilities.SpreadSheetTestData;

@RunWith(Parameterized.class)
public class liveExcelSpreadsheetTest2 {
	
	@Parameters
	public static Collection data() throws IOException{
		InputStream spreadSheet = new FileInputStream("C:\\Automation\\Projects\\automation_with_java_repo\\FirstBasicFramework\\FirstBasicFramework\\src\\test\\resources\\SprintOne\\SprintOneLoginTestData.xls");
		return new SpreadSheetTestData(spreadSheet).getData();
	}
	
	private WebDriver driver;
	private AbstractPage abstractPage;
	private HomePage homePage;
	private MyAccountPage myAccountPage;
	private WelcomePage welcomePage;
	private SearchResultPage searchResultPage;
	
	private String username;
	private String password;
	
	public liveExcelSpreadsheetTest2 (String username, String password){
		this.username = username;
		this.password = password;
	}



@Before
public void setUp() throws Exception{
	driver = new FirefoxDriver();
	abstractPage = new AbstractPage(driver);
	homePage = new HomePage(driver);
	myAccountPage = new MyAccountPage(driver);
	welcomePage = new WelcomePage(driver);		
	searchResultPage = new SearchResultPage(driver);
		
	abstractPage.openAndMaximizeBrowser();

}

@Test
public void loginTestExcelSheet(){
	try{	
	homePage.openMyAccount();
	myAccountPage.loginWith(username, password);
	welcomePage.verifyLogin();
	}
	catch(Exception e){
		e.printStackTrace();
	}	
}


@After
public void closeBrowser(){
	abstractPage.tearDownBrowser();
}
}



