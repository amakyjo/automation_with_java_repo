package com.live.magento.test_area;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.live.magento.pages.AbstractPage;
import com.live.magento.pages.HomePage;
import com.live.magento.pages.MyAccountPage;
import com.live.magento.pages.SearchResultPage;
import com.live.magento.pages.WelcomePage;
import com.live.magento.utilities.SpreadSheetTestData;

@RunWith(Parameterized.class)
public class liveExcelSpreadSheetSearchTest {
	@Parameters
	public static Collection data() throws IOException{
		InputStream spreadSheet = new FileInputStream("C:\\Automation\\Projects\\automation_with_java_repo\\FirstBasicFramework\\FirstBasicFramework\\src\\test\\resources\\SprintOne\\SprintOneSearchTestData.xls");
		return new SpreadSheetTestData(spreadSheet).getData();
	}
	
	private WebDriver driver;
	private AbstractPage abstractPage;
	private HomePage homePage;
	private MyAccountPage myAccountPage;
	private WelcomePage welcomePage;
	private SearchResultPage searchResultPage;
	

	private String productName;
	
	public liveExcelSpreadSheetSearchTest (String productName){
		this.productName = productName;
		
	}



@Before
public void setUp() throws Exception{
	driver = new FirefoxDriver();
	abstractPage = new AbstractPage(driver);
	homePage = new HomePage(driver);
	myAccountPage = new MyAccountPage(driver);
	welcomePage = new WelcomePage(driver);		
	searchResultPage = new SearchResultPage(driver);
		
	abstractPage.openAndMaximizeBrowser();

}
@Test
public void searchingForProducts(){
	abstractPage.searchForProduct(productName);
	searchResultPage.verifyProduct(productName);
}


@After
public void closeBrowser(){
	abstractPage.tearDownBrowser();
}

}
