package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MobileProductPage extends AbstractPage{
	
	public MobileProductPage(WebDriver driver) {
		super(driver);
	}

	public void verifyPageAndOpenProductPage(){
		try{
			logger.info("The verifyPageAndOpenProductPage method has been initiated");
			boolean result = driver.getPageSource().contains("This is root of mobile");
			Assert.assertTrue("Test failed because the page does not contain This is root of mobile ", result);
			logger.info("mobile page has been verified successfully");
			
			if(result == true){
				driver.findElement(By.id("product-collection-image-1")).click();
				logger.info("mobile product link has been clicked successfully");		
				}
		}catch(Throwable t){
			logger.error("The verifyPageAndOpenProductPage method has encountered error" +t);
			camera.takeShot("verifyPageAndOpenProductPage");
		}
		
	}

}
