package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class ShoppingCartPage extends AbstractPage {

	public ShoppingCartPage(WebDriver driver) {
		super(driver);
	}
	
	public void verifyProductAddedToCart(){
		String message = "was added to your shopping cart";
		try{
			logger.info("The verifyProductAddedToCart method has started successfully");
			boolean cartResult = driver.getPageSource().contains("message");
			Assert.assertTrue("Test failed because the page does not contain"+message, cartResult);
			logger.info("Product added to cart has been verified");
		}catch(Throwable t){
			logger.error("The method verifyProductAddedToCart has encountered error" +t);
			camera.takeShot("verifyProductAddedToCart");
			
		}
	}

	
}
