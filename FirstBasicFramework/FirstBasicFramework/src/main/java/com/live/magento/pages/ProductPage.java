package com.live.magento.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductPage extends AbstractPage{

	public ProductPage(WebDriver driver) {
		super(driver);
	}
	
	public void addProductToCart(){
		try{
			logger.info("The addProductToCart method has been initiated");
			driver.findElement(By.id("qty")).clear();
			driver.findElement(By.id("qty")).sendKeys("1");
			driver.findElement(By.cssSelector(".button.btn-cart")).click();
			logger.info("Product has been successfully added to the cart");	
		}catch(Throwable t){
			logger.error("The addProductToCart method has encountered error" +t);
			camera.takeShot("addProductToCart");
		}
	}

}
