package com.aerlingus.release_one.test_suite_steps;

import java.util.List;


import com.aerlingus.page_objects.HolidayPage;
import com.aerlingus.page_objects.SearchResultPage;
import com.aerlingus.support.WorldHelper;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ValidateHolidayBookingStepDef {
	
	private WorldHelper helper;
	private HolidayPage holidayPage;
	private SearchResultPage searchResultPage;


	
	public ValidateHolidayBookingStepDef(WorldHelper helper){
		this.helper = helper;
	}
	@Given("^that a customer is on the Holiday booking page$")
	public void that_a_customer_is_on_the_Holiday_booking_page() throws Throwable {
		
		holidayPage = helper.getHomePage().gotoHoliday().selectHolidayDestination().bookHoliday();
	   
	}

	@When("^he search for flight and hotel with the following data populated:$")
	public void he_search_for_flight_and_hotel_with_the_following_data_populated(DataTable bookingDetailsTable) throws Throwable {
		List<List<String>>bookingDetails = bookingDetailsTable.raw();
		String from = bookingDetails.get(1).get(0);
		String to = bookingDetails.get(1).get(1);
		String departureDate = bookingDetails.get(1).get(2);
		String numOfNights = bookingDetails.get(1).get(3);
		String numOfRooms = bookingDetails.get(1).get(4);
		String numOfAdults = bookingDetails.get(1).get(5);
		String numOfChildren = bookingDetails.get(1).get(6);
		String childAge = bookingDetails.get(1).get(7);
		String numOfInfants = bookingDetails.get(1).get(8);
		String infantAge = bookingDetails.get(1).get(9);
		
		searchResultPage = holidayPage
				.populateSearchDetails(from, to, departureDate, numOfNights, numOfRooms, numOfAdults, numOfChildren, childAge, numOfInfants, infantAge)
				.search();
		
		
	}

	@Then("^he should find the following information:$")
	public void he_should_find_the_following_information(DataTable searchResultTable) throws Throwable {
		List<List<String>>searchResult = searchResultTable.raw();
		String title = searchResult.get(1).get(0);
		String content1 = searchResult.get(1).get(1);
		String content2 = searchResult.get(1).get(2);
		String travelDate = searchResult.get(1).get(3);
	
		
		searchResultPage.verifySearchResult(title, content1, content2, travelDate);
	 
	}
	
}
