package com.aerlingus;




import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

      
@RunWith(Cucumber.class)
@CucumberOptions(
		monochrome = true
	   ,plugin = {"pretty",
					"html:target/test-report/report-xml",
					"json:target/test-report/report-json.json",
					"junit:target/test-report/report-xml.xml"}      //Report Template
	   ,strict = false		 										//Skip execution of pending and undefined steps if true
	   ,features = {"src/test/resources/"}		//Packages where the feature files are located
//	   ,glue = {"src/test/java/"}  									//Package with Step Definitions and hooks
	   ,tags = {"@wip"}									//Feature file hooks
		)
public class RunnerCukesTest {}

