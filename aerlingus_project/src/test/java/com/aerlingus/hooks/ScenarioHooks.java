package com.aerlingus.hooks;



import com.aerlingus.browsers.Browsers;


import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;


public class ScenarioHooks {
	
	private Browsers browser;

	public ScenarioHooks(Browsers browser) {
		this.browser = browser;
	}
	
	

	@Before
	public void setUpEnvironment() throws Throwable {
//		System.out.println("Hello Startup AB");
		browser.instatiateDriver().setUpCleanDriver().loadDefaultBasePage();
	}
								
				
	@After
	public void tearDownEnvironment(Scenario scenario) {
		if(scenario.isFailed()){
			scenario.write("Current status is " + scenario.getStatus());
			System.out.println("There is a failure on the scenario");
		}
		scenario.write("Current status is " + scenario.getStatus());
		System.out.println(scenario.getStatus());
		browser.destroyDriver();
	}
	
	
	
//	@After("@database") 
//	public void lastTest() throws MessagingException{
//		System.out.println("Database connection closing...");
//		browser.sendEmail();
//		browser.closeDatabaseConnection();
//	}
//	
//	
//	@Before("@database")
//	public void overAllstart() throws MessagingException{
//		System.out.println("Database connection opening...");
//		browser.openDatabase();
//	}

}
