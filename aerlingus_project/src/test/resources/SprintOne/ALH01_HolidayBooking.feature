@wip
Feature: Testing Aer Lingus Web Application to validate Holiday Search functionality
  As an Aer Lingus customer
  I would like to search for holiday resorts on the online application
  So I can book my desired destination

  Scenario: Validate holiday search functionality with field populated with valid information
    Given that a customer is on the Holiday booking page
    When he search for flight and hotel with the following data populated:
      | From                 | To            | Departure Date | Nights | Rooms | Adults | Children | child age | Infants | infant age |
      | London Gatwick (LGW) | New York City | 25 Jan 2016    | 7      | 1     | 2      | 1        | 2         | 1       | 1          |
    Then he should find the following information:
      | Title              | content1    | content2       | Actual Travel day       |
      | aerlingus-holidays | You require | London Gatwick | Monday, 25 January 2016 |
