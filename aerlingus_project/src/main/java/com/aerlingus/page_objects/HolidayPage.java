package com.aerlingus.page_objects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HolidayPage extends BasePage{
	
	
//	Populate the Hotel and Flight Search fields
	@FindBy(id="sb3_sb3_fh_sb3_flight_iataFrom")
	private WebElement fromField;
	
	@FindBy(id="sb3_sb3_fh_sb3_hotel_city")
	private WebElement toField;

	@FindBy(id="sb3_sb3_fh_sb3_flight_outboundDate")
	private WebElement departureDateField;
	
	@FindBy(id="sb3_sb3_fh_sb3_hotel_numNights")
	private WebElement numOfNightsField;
	
	@FindBy(id="sb3_sb3_fh_sb3_hotel_numRooms")
	private WebElement numOfRoomsField;
	
	@FindBy(id="sb3_sb3_fh_sb3_fh_numAdults1")
	private WebElement adultsField;
	
	@FindBy(id="sb3_sb3_fh_sb3_fh_numChildren1")
	private WebElement childrenField;
	
	@FindBy(id="sb3_sb3_fh_sb3_flighthotel_childAge1_1")
	private WebElement childAgeField;
	
	@FindBy(id="sb3_sb3_fh_sb3_fh_numInfants1")
	private WebElement infantField;
	
	@FindBy(id="sb3_sb3_fh_sb3_flighthotel_infantAge1_1")
	private WebElement infantAgeField;
	
	@FindBy(id="sb3_sb3_fh_sb3_flighthotel_search_button")
	private WebElement searchBtn;
	
	public HolidayPage(WebDriver driver) {
		super(driver);
		
	}

	public HolidayPage populateSearchDetails(String from, String to, String departureDate, String numOfNights,
			String numOfRooms, String numOfAdults, String numOfChildren, String childAge, String numOfInfants, String infantAge) {
				
		try{
			logger.info("The populateSearchDetails method started execution");
			
			Select selectFrom = new Select(fromField);
			selectFrom.selectByVisibleText(from);
			toField.clear();
			toField.sendKeys(to);
			departureDateField.clear();
			departureDateField.sendKeys(departureDate);
			numOfNightsField.clear();
			numOfNightsField.sendKeys(numOfNights);
			Select selectNumOfRooms = new Select(numOfRoomsField);
			selectNumOfRooms.selectByValue(numOfRooms);
			Select selectAdults = new Select(adultsField);
			selectAdults.selectByValue(numOfAdults);
			Select selectChildren = new Select(childrenField);
			selectChildren.selectByValue(numOfChildren);
			Select selectChildAge = new Select(childAgeField);
			selectChildAge.selectByValue(childAge);
			Select selectInfant = new Select(infantField);
			selectInfant.selectByValue(numOfInfants);
			Select selectInfantAge = new Select(infantAgeField);
			selectInfantAge.selectByValue(infantAge);
			logger.info("The fields have all been populated successfully ");
		}catch(Throwable t){
			camera.takeShot("populateSearchDetails");
			logger.error("The populateSearchDetails method encountered an error "+t);
		}
		return PageFactory.initElements(driver, HolidayPage.class);	
	}

	public SearchResultPage search() {
		try{
			logger.info("The search method has started successfully");
			searchBtn.click();
			logger.info("The flight and hotel has been searched successfully ");
			logger.info("The search method executed successfully");
		}catch(Throwable t){
			camera.takeShot("search");
			logger.error("The search method encountered an error "+t);
		}
		return PageFactory.initElements(driver, SearchResultPage.class);
	}
	

}
