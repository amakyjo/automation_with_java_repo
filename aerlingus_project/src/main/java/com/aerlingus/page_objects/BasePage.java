package com.aerlingus.page_objects;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.aerlingus.utilities.Screenshot;
import com.aerlingus.utilities.UrlFormatter;





public class BasePage{
	
	protected WebDriver driver;
	protected static Properties config = new Properties();
	protected static InputStream fis;
	protected Screenshot camera;
	protected static Logger logger = LogManager.getLogger("TestLogger");
	
	
	public BasePage(WebDriver driver){
		this.driver = driver;
		camera = new Screenshot(driver);
		
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+ "\\src\\main\\resources\\ObjectRepo.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			config.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
	public HomePage loadDefaultBasePage() {
		try{
			logger.info("The loadDefaultBasePage method is started correctly");
			String formatedUrl = UrlFormatter.formatUrl(config.getProperty("base_url"));
			logger.info("The Url has been formatted correctly");
			driver.navigate().to(formatedUrl);
			logger.info("The Web address has been loaded successfully");
		}catch(Throwable t){
			logger.error("The loadDefaultBasePage Method has encountered error" +t);
			camera.takeShot("loadDefaultBasePage");
		}
		return PageFactory.initElements(driver, HomePage.class);
		
	}
	
	
	

}
