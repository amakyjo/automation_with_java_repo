package com.aerlingus.page_objects;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchResultPage extends BasePage{
	
	
	@FindBy(id="sb3_sb3_fh_sb3_flighthotel_search_button")
	private WebElement selectHotelBtn;
	
	@FindBy(xpath="html/body/div[1]/div[1]/div[2]/div[1]/div[3]/button")
	private WebElement textMessage;
	

	public SearchResultPage(WebDriver driver) {
		super(driver);
	}



	public SearchResultPage verifySearchResult(String title, String content1, String content2, String travelDate) {
		
		try{
			logger.info("The verifySearchResult method has started ");
			WebDriverWait wait = new WebDriverWait(driver, 180);
			wait.until(ExpectedConditions.visibilityOf(textMessage));
			
			Assert.assertTrue(title, driver.getCurrentUrl().contains(title));
			logger.info("The presence of "+title+" has been verified");
			
			Assert.assertFalse(content1, !driver.getPageSource().contains(content1));
			logger.info("The presence of "+content1+" has been verified");
			
			Assert.assertTrue(content2, driver.getPageSource().contains(content2));
			logger.info("The presence of "+content2+" has been verified");
			
			Assert.assertTrue(travelDate, driver.getPageSource().contains(travelDate));
			logger.info("The presence of "+travelDate+" has been verified");
			logger.info("The messages has been verified successfully ");
		}catch(Throwable t){
			camera.takeShot("verifySearchResult");
			logger.error("The verifySearchResult method encountered an error "+t);
		}
		
		return PageFactory.initElements(driver, SearchResultPage.class);
	}
	
	public BookingConfirmationPage selectHotel() {
		try{
			logger.info("The search method has started successfully");
			selectHotelBtn.click();
			driver.manage().timeouts().implicitlyWait(25,TimeUnit.SECONDS);
			logger.info("The flight and hotel has been searched successfully ");
			logger.info("The search method executed successfully");
		}catch(Throwable t){
			camera.takeShot("search");
			logger.error("The search method encountered an error "+t);
		}
		return PageFactory.initElements(driver, BookingConfirmationPage.class);
		
	}

}
