package com.aerlingus.page_objects;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class BookingConfirmationPage extends BasePage {

	public BookingConfirmationPage(WebDriver driver) {
		super(driver);
	
	}

	public void verifyConfirmationDetails(String title, String info1, String info2) {
		
		try{
			logger.info("The verifyConfirmationDetails method has started ");
			Assert.assertTrue(title, driver.getPageSource().contains(title));
			logger.info("The presence of "+title+" has been verified");
			
			Assert.assertFalse(info1, !driver.getPageSource().contains(info1));
			logger.info("The presence of "+info1+" has been verified");
			
			Assert.assertTrue(info2, driver.getPageSource().contains(info2));
			logger.info("The presence of "+info2+" has been verified");
			logger.info("The messages has been verified successfully ");
		}catch(Throwable t){
			camera.takeShot("verifyConfirmationDetails");
			logger.error("The verifyConfirmationDetails method encountered an error "+t);
		}
		
	}

}
