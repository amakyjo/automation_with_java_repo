package com.aerlingus.page_objects;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class HomePage extends BasePage{
	

	
//	open holiday tab
	@FindBy(xpath="html/body/div[2]/div[2]/div[2]/div/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[1]/div/div/div[3]")
	private WebElement holidayTab;
	
//	choose holiday destination
	@FindBy(xpath = "html/body/div[2]/div[2]/div[2]/div/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[2]/div[3]/div[1]/section/div/div/div/ol/li[2]")
	private WebElement holidayDest;
	
// book selected holiday destination
	@FindBy(xpath = "html/body/div[2]/div[2]/div[2]/div/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[2]/div[3]/div[1]/section/div/div/div/div/div[2]/div/a")
	private WebElement bookBtn;
		
	public HomePage(WebDriver driver) {
		super(driver);
	}


	public HomePage gotoHoliday() {
		try{
			logger.info("The gotoHoliday method started with data: ");
			WebDriverWait wait = new WebDriverWait(driver, 120);
			WebElement element = wait.until(ExpectedConditions.visibilityOf(holidayTab));
			element.click();
			logger.info("The holiday tab has been clicked successfully ");
			logger.info("The gotoHoliday method executed successfully");
		}catch(Throwable t){
			camera.takeShot("gotoHoliday");
			logger.error("The gotoHoliday method encountered an error "+t);
		}
		return PageFactory.initElements(driver, HomePage.class);
	}




	public HomePage selectHolidayDestination() {
		try{
			logger.info("The selectHolidayDestination method started with data: ");
			WebDriverWait wait = new WebDriverWait(driver, 120);
			WebElement element = wait.until(ExpectedConditions.visibilityOf(holidayDest));
			element.click();
			logger.info("The destination has been selected successfully ");
			logger.info("The selectHolidayDestination method executed successfully");
		}catch(Throwable t){
			camera.takeShot("selectHolidayDestination");
			logger.error("The selectHolidayDestination method encountered an error "+t);
		}
		return PageFactory.initElements(driver, HomePage.class);
	}



	public HolidayPage bookHoliday() {
		try{
			logger.info("The bookHoliday method started with data: ");
			bookBtn.click();
			for(String newWindow:driver.getWindowHandles()){
				driver.switchTo().window(newWindow);
			}
			logger.info("The flight has been searched successfully ");
			logger.info("The bookHoliday method executed successfully");
		}catch(Throwable t){
			camera.takeShot("bookHoliday");
			logger.error("The bookHoliday method encountered an error "+t);
		}
		return PageFactory.initElements(driver, HolidayPage.class);
		
	}




	



}
