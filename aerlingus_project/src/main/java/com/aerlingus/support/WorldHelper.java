package com.aerlingus.support;

import org.openqa.selenium.support.PageFactory;

import com.aerlingus.browsers.Browsers;
import com.aerlingus.page_objects.BasePage;
import com.aerlingus.page_objects.HolidayPage;
import com.aerlingus.page_objects.HomePage;
import com.aerlingus.page_objects.SearchResultPage;




public class WorldHelper extends Browsers{
	
	private HomePage homePage;
	private BasePage basePage;
	private SearchResultPage searchResultPage;
	private HolidayPage holidayPage;
	
	

	
	public BasePage getBasePage() {
		if(basePage == null){
			basePage = PageFactory.initElements(driver, BasePage.class);
		}
		return basePage;
	}
	
	
	public SearchResultPage getSearchResultPage() {
		if(searchResultPage == null){
			searchResultPage = PageFactory.initElements(driver, SearchResultPage.class);
		}
		return searchResultPage;
	}
	
	public HolidayPage getHolidayPage() {
		if(holidayPage == null){
			holidayPage = PageFactory.initElements(driver, HolidayPage.class);
		}
		return holidayPage;
	}
	
	public HomePage getHomePage() {
		if(homePage == null){
			homePage = PageFactory.initElements(driver, HomePage.class);
		}
		return homePage;
	}
	
	


	
	
	

}
