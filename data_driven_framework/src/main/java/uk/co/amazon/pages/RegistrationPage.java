package uk.co.amazon.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RegistrationPage extends AbstractPage{
	
	@FindBy(id="ap_customer_name")
	private WebElement nameField;
	
	@FindBy(id="ap_email")
	private WebElement emailField;
	
	@FindBy(id="ap_email_check")
	private WebElement emailField2;
	
	@FindBy(id="ap_password")
	private WebElement passwordField;
	
	@FindBy(id="ap_password_check")
	private WebElement passwordField2;
	
	@FindBy(id="continue-input")
	private WebElement createAcctBtn;

	public RegistrationPage(WebDriver driver) {
		super(driver);
		
	}

	
	public UserAccountPage registerWithInformation(String name,String email1, String email2, String password1, String password2) {
		
		try{
			logger.info("The registerWithInformation method has started successfully");
			nameField.sendKeys(name);
			logger.info("name has been entered successfully");
			emailField.sendKeys(email1);
			emailField2.sendKeys(email2);
			logger.info("email has been entered successfully");
			passwordField.sendKeys(password1);
			passwordField2.sendKeys(password2);
			logger.info("password has been entered successfully");
			createAcctBtn.click();
			logger.info("my account has been clicked");
		}catch(Throwable t){
			logger.error("The method registerWithInformation has encountered error" +t);
			camera.takeShot("registerWithInformation");
		}

		return PageFactory.initElements(driver, UserAccountPage.class);
	}

}
