package uk.co.amazon.pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import uk.co.amazon.utilities.Screenshot;
import uk.co.amazon.utilities.UrlFormatter;



public class AbstractPage {

	protected WebDriver driver;
	protected static Properties config = new Properties();
	protected static InputStream fis;
	protected Screenshot camera;
	protected static Logger logger = LogManager.getLogger("TestLogger");
	
	@FindBy(id="nav-link-yourAccount")
	private WebElement regHover;
	@FindBy(linkText="Start here.")
	private WebElement regLinkElement;
	
	public AbstractPage(WebDriver driver){
		this.driver = driver;
		this.camera = new Screenshot(driver);
		
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+ "\\src\\main\\resources\\testData.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			config.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public RegistrationPage openRegistrationPage() {
		try{
			logger.info("The openRegistrationPage method has started successfully");
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			Actions action = new Actions(driver);
			action.moveToElement(regHover).perform();
			action.moveToElement(regLinkElement).click().build().perform();
			logger.info("user registration link has been opened successfully");
		}catch(Throwable t){
			logger.error("The method openRegistrationPage has encountered error" +t);
			camera.takeShot("openRegistrationPage");
		}
		return PageFactory.initElements(driver, RegistrationPage.class);
	
	}
	public void openAndMaximizeBrowser() {
		try{
			logger.info("The openAndMaximiseBrowser method is started correctly");
			String formatedUrl = UrlFormatter.formatUrl(config.getProperty("baseUrl"));
			logger.info("The Url has been formatted correctly");
			driver.navigate().to(formatedUrl);
			logger.info("The Web address has been loaded successfully");
		}catch(Throwable t){
			logger.error("The openAndMaximizeBrowser Method has encountered error" +t);
			camera.takeShot("openAndMaximizeBrowser");
		}
		
	}


}
