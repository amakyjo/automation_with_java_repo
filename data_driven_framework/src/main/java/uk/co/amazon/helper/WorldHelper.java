package uk.co.amazon.helper;


import org.openqa.selenium.support.PageFactory;



import uk.co.amazon.browsers.Browsers;
import uk.co.amazon.pages.AbstractPage;
import uk.co.amazon.pages.HomePage;




public class WorldHelper extends Browsers{
	
	private AbstractPage abstractPage;
	private HomePage homePage;
	

	
	public AbstractPage getAbstractPage(){
		if(abstractPage == null){
			abstractPage = PageFactory.initElements(driver, AbstractPage.class);
		}
		return abstractPage;
	}
	
	public HomePage getHomePage(){
		if(homePage == null){
			homePage = PageFactory.initElements(driver, HomePage.class);
		}
		return homePage;
	}

}
