package co.uk.amazon.test_env;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import uk.co.amazon.excel.utils.GenericExcelReader;
import uk.co.amazon.helper.WorldHelper;

@RunWith(DataProviderRunner.class)
public class TestUserRegistration {
	
	
	
	private WorldHelper helper;
	private static GenericExcelReader excel = new GenericExcelReader(System.getProperty("user.dir")+"\\src\\test\\resources\\SprintOne\\UserRegistrationInfo.xlsx");

	@Before
	public void setUp() throws Exception{
		helper = new WorldHelper();
		helper.instatiateDriver();
		helper.setUpCleanDriver().openAndMaximizeBrowser();
		//helper.instatiateDriver().setUpCleanDriver();
		
	}
	
	
	@Test @UseDataProvider("testData")
	public void testUserRegistrationFunctionality(String name, String email1, String email2, String password1, String password2){
		
		helper.getAbstractPage()
		.openRegistrationPage()
		.registerWithInformation(name, email1, email2, password1, password2)
		.verifyUserAccount();
			
	}
	
	@After
	public void closeBrowser(){
		helper.destroyDriver();
	}
	
	
	@DataProvider
	public static Object[][] testData(){
		String sheetName = "sheet1";
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);
		Object[][] data = new Object[rows-1][cols];
		for(int rowNum = 2 ; rowNum <= rows ; rowNum++){ //2
			for(int colNum=0 ; colNum< cols; colNum++){
				data[rowNum-2][colNum]=excel.getCellData(sheetName, colNum, rowNum); //-2
			}
		}
		return data;
	}

}
