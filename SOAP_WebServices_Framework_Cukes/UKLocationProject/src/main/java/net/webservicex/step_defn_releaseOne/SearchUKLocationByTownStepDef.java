package net.webservicex.step_defn_releaseOne;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.webservicex.UKLocationSoap;
import net.webservicex.support.WorldHelper;

public class SearchUKLocationByTownStepDef {
	
	private WorldHelper helper;
	private UKLocationSoap uKLocationSoap;
	private String townFromServer;

	public SearchUKLocationByTownStepDef(WorldHelper helper){
		this.helper = helper;
		
	}

	
	@Given("^that I have access to the webserver$")
	public void that_I_have_access_to_the_webserver() throws Throwable {
	   uKLocationSoap = helper.getUKLocation().getUKLocationSoap();
	}

	@When("^search for item \"([^\"]*)\"$")
	public void search_for_item(String townName) throws Throwable {
	
		townFromServer = uKLocationSoap.getUKLocationByTown(townName);
	}

	@Then("^the following information is retrieved: \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void the_following_information_is_retrieved_and(String townName, String countyName, String postCode) throws Throwable {
	   Assert.assertTrue("Does not contain " +townName, townFromServer.contains(townName));
	   Assert.assertTrue("Does not contain " +countyName, townFromServer.contains(countyName));
	   Assert.assertTrue("Does not contain " +postCode, townFromServer.contains(postCode));
	}
}
