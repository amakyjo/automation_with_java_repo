@Regression @InSprint
Feature: Testing for search town functionality
  As a customer(back-end Tester)
  I want to search for UK town 
  So I can plan my journey to the town

  Scenario Outline: Testing for search functionality item name
    Given that I have access to the webserver
    When search for item "<Town Name>"
    Then the following information is retrieved: "<Town Name>", "<County Name>" and "<Town PostCode>"

    Examples: Data Needed
      | Town Name            | County Name    | Town PostCode |
      | Sunderland           | Cumbria        | CA13          |
      | Sunderland           | Tyne and Wear  | SR1           |
      | Sunderland Bridge    | County Durham  | DH6           |
      | North Sunderland     | Northumberland | NE68          |
      | Little Newcastle     | Pembrokeshire  | SA62          |
      | Newcastle            | Bridgend       | CF31          |
      | Newcastle            | Monmouthshire  | NP5           |
      | Newcastle-under-Lyme | Staffordshire  | ST5           |
      | Sunderland           | Cumbria        | CA13          |
      | Sunderland           | Tyne and Wear  | SR1           |
      | Sunderland Bridge    | County Durham  | DH6           |
      | North Sunderland     | Northumberland | NE68          |
      | Little Newcastle     | Pembrokeshire  | SA62          |
      | Newcastle            | Bridgend       | CF31          |
      | Newcastle            | Monmouthshire  | NP5           |
      | Newcastle-under-Lyme | Staffordshire  | ST5           |
      | Sunderland           | Cumbria        | CA13          |
      | Sunderland           | Tyne and Wear  | SR1           |
      | Sunderland Bridge    | County Durham  | DH6           |
      | North Sunderland     | Northumberland | NE68          |
      | Little Newcastle     | Pembrokeshire  | SA62          |
      | Newcastle            | Bridgend       | CF31          |
      | Newcastle            | Monmouthshire  | NP5           |
      | Newcastle-under-Lyme | Staffordshire  | ST5           |
      | Sunderland           | Cumbria        | CA13          |
      | Sunderland           | Tyne and Wear  | SR1           |
      | Sunderland Bridge    | County Durham  | DH6           |
      | North Sunderland     | Northumberland | NE68          |
      | Little Newcastle     | Pembrokeshire  | SA62          |
      | Newcastle            | Bridgend       | CF31          |
      | Newcastle            | Monmouthshire  | NP5           |
      | Newcastle-under-Lyme | Staffordshire  | ST5           |
