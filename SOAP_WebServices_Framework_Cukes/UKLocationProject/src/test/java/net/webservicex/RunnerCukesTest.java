package net.webservicex;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(     
			dryRun = false 		//Does not run any code content in the step definition when it is true
		   ,monochrome = true 	//Make text on the console readable
		   ,plugin = {"pretty",
						"html:target/test-report/report-html",
						"json:target/test-report/report-json.json",
						"junit:target/test-report/report-xml.xml"}      //Report Template
		   ,strict = false		 										//Skip execution of pending and undefined steps if true
		   ,features = {"src/test/resources"}		//Packages where the feature files are located
		   ,snippets = SnippetType.CAMELCASE		//Used to determine what the method of the Step Def would be
//		   ,glue = {"com.live.magento.step_definitions", ""}  			//Package with Step Definitions and hooks
		   ,tags = {"@Regression"}					//Used to determine which test would run
			)
public class RunnerCukesTest {}
