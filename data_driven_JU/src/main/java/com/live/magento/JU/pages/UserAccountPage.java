package com.live.magento.JU.pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class UserAccountPage extends AbstractPage{

	public UserAccountPage(WebDriver driver) {
		super(driver);
		
	}

	public void verifyUserAccount() {
		try{
			logger.info("The verifyUserAccount method has started successfully");
			boolean result = driver.getPageSource().contains(config.getProperty("message"));
			Assert.assertTrue("Test failed because the page does not contain "+ config.getProperty("message"), result);
			logger.info("message has been verified");
		}catch(Throwable t){
			logger.error("The method verifyUserAccount has encountered error" +t);
			camera.takeShot("verifyUserAccount");
			
		}
		
	}

}
