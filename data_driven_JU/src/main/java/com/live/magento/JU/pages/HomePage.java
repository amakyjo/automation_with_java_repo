package com.live.magento.JU.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.live.magento.JU.pages.MyAccountPage;



public class HomePage extends AbstractPage{

	@FindBy(xpath="html/body/div[1]/div/div[3]/div/div[4]/ul/li[1]/a")
	private WebElement accountLink;
	

	public HomePage(WebDriver driver) {
		super(driver);
	}
	

	public MyAccountPage openMyAccount(){
		try{
			logger.info("The openMyAccount method has started successfully");
			accountLink.click();
			logger.info("Account has been opened successfully");
		}catch(Throwable t){
			logger.error("The method openMyAccount has encountered error" +t);
			camera.takeShot("openMyAccount");
		}
		return PageFactory.initElements(driver, MyAccountPage.class);
	
	}
}
