package com.live.magento.JU.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends AbstractPage {
	
	@FindBy(xpath = "html/body/div[1]/div/div[2]/div/div/div/form/div/div[1]/div[2]/a")
	private WebElement createAccountLink;
	
	public MyAccountPage(WebDriver driver) {
		super(driver);
		
	}

	public RegistrationPage createAccount(){
		try{
			logger.info("The createAccount method has started successfully");
			createAccountLink.click();
			logger.info("create account link has been clicked successfully");
		}catch(Throwable t){
			logger.error("The method createAccount has encountered error" +t);
			camera.takeShot("createAccount");
		}
		return PageFactory.initElements(driver, RegistrationPage.class);
	
	}
}
