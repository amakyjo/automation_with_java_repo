package com.live.magento.JU.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class RegistrationPage extends AbstractPage{
	
	@FindBy(id="firstname")
	private WebElement firstNameField;
	@FindBy(id="lastname")
	private WebElement lastNameField;
	@FindBy(id="email_address")
	private WebElement emailField;
	
	@FindBy(id="password")
	private WebElement passwordField;
	
	@FindBy(id="confirmation")
	private WebElement passwordConfirmField;
	
	@FindBy(xpath="html/body/div[1]/div/div[2]/div/div/div/form/div[2]/button")
	private WebElement registerBtn;

	public RegistrationPage(WebDriver driver) {
		super(driver);
		
	}

	
	public UserAccountPage registerWithInformation(String firstName, String lastName, String email, String password1, String password2) {
		
		try{
			logger.info("The registerWithInformation method has started successfully");
			firstNameField.sendKeys(firstName);
			logger.info(" first name has been entered successfully");
			lastNameField.sendKeys(lastName);
			logger.info("name has been entered successfully");
			emailField.sendKeys(email);
			passwordField.sendKeys(password1);
			passwordConfirmField.sendKeys(password2);
			logger.info("password has been entered successfully");
			registerBtn.click();
			logger.info("my account has been clicked");
		}catch(Throwable t){
			logger.error("The method registerWithInformation has encountered error" +t);
			camera.takeShot("registerWithInformation");
		}

		return PageFactory.initElements(driver, UserAccountPage.class);
	}

}
