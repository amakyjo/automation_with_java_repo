package com.live.magento.JU.helper;

import org.openqa.selenium.support.PageFactory;

import com.live.magento.JU.browsers.Browsers;
import com.live.magento.JU.pages.HomePage;





public class WorldHelper extends Browsers{
	
	private HomePage homePage;;
	
	
	public HomePage getHomePage(){
		if(homePage == null){
			homePage = PageFactory.initElements(driver, HomePage.class);
		}
		return homePage;
	}
	
	
}
