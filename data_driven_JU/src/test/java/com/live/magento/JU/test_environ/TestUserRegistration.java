package com.live.magento.JU.test_environ;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.live.magento.JU.excel.utils.GenericExcelReader;
import com.live.magento.JU.helper.WorldHelper;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
;

@RunWith(DataProviderRunner.class)
public class TestUserRegistration {
	
	
	private WorldHelper helper;
	private static GenericExcelReader excel = new GenericExcelReader(System.getProperty("user.dir")+"\\src\\test\\resources\\SprintOne\\UserRegistrationInfo.xlsx");

	@Before
	public void setUp() throws Exception{
		helper = new WorldHelper();
		helper.instatiateDriver();
		helper.setUpCleanDriver().loadDefaultPage();
	}
	
	
	@Test @UseDataProvider("testData")
	public void testUserRegistrationFunctionality(String firstName, String lastName, String email, String password1, String password2){
		helper.getHomePage()
		.openMyAccount()
		.createAccount().registerWithInformation(firstName, lastName, email, password1, password2)
		.verifyUserAccount();
				
	}
	
	
	@DataProvider
	public static Object[][] testData(){
		String sheetName = "sheet1";
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);
		Object[][] data = new Object[rows-1][cols];
		for(int rowNum = 2 ; rowNum <= rows ; rowNum++){ //2
			for(int colNum=0 ; colNum< cols; colNum++){
				data[rowNum-2][colNum]=excel.getCellData(sheetName, colNum, rowNum); //-2
			}
		}
		return data;
	}
	
	@After
	public void closeBrowser(){
		helper.destroyDriver();
	}

}
