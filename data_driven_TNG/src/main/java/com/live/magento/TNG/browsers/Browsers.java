package com.live.magento.TNG.browsers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import com.live.magento.TNG.pages.AbstractPage;






public class Browsers {
	

	protected static WebDriver driver;
	protected static Properties config = new Properties();
	protected static InputStream fis;
	protected static Logger logger = LogManager.getLogger("TestLogger");


	public WebDriver instatiateDriver(){
		String systemPath = System.getProperty("user.dir");
		if(driver == null){
			logger.info("The initializeDriver method has started execution as the browser is null");			
			try {
				fis = new FileInputStream(systemPath+"\\src\\main\\resources\\testData.properties");
				logger.info("Path for the Object repository has been loaded successfully");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			try {
				config.load(fis);
				logger.info("config.load(fis) has been loaded successfully for browser details");
			} catch (IOException e) {
				e.printStackTrace();
			} 
			logger.info("Loaded the Locator database property file");
			
			if(config.getProperty("browser").equalsIgnoreCase("firefox")){
				driver = new FirefoxDriver();
				logger.info("Executed test on the Firefox Browser");	
			}else if(config.getProperty("browser").equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", systemPath+"\\drivers\\chromedriver.exe");
				driver = new ChromeDriver();
				logger.info("Executed test on the Chrome Browser");	
			}else if(config.getProperty("browser").equalsIgnoreCase("ghost_html")){
					driver = new HtmlUnitDriver();
					logger.info("Executed test on the ghost browser Browser");	
			}else if(config.getProperty("browser").equalsIgnoreCase("iexplorer")){
				System.setProperty("webdriver.ie.driver", systemPath+"\\drivers\\IEDriverServer.exe");
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
				caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				caps.setCapability("nativeEvents", false);
				driver = new InternetExplorerDriver(caps);
				logger.info("Executed test on the Internet Exprorer Browser");	
				}
			else{
				logger.info("There is no such browser in this application");
				throw new IllegalStateException("There is no such browser in this application");
			}		
		}
		return driver;
	}
	
	
	public AbstractPage setUpCleanDriver(){
		try{
			driver.manage().window().maximize();
			logger.info("Window has been maximized");
			driver.manage().deleteAllCookies();
			logger.info("All cookies are cleared and Browser is clean");
			driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
			logger.info("Implicit wait now applies");
		}catch(Exception e){
			logger.info("Unable to setup a clean new browser");
		}		
		return PageFactory.initElements(driver, AbstractPage.class);
	}
	
	
		
	public void destroyDriver(){
		try {
			driver.quit();
			logger.info("The browser has now quit");
			driver = null;
			logger.info("The browser has no value anymore");
		}catch(Throwable t) {
			logger.error("The destroyDriver method has encountered error", t);
		}

	
	
	
	}
}

