package com.live.magento.TNG.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class ContactUsPage extends AbstractPage{
	
	@FindBy(id="name")
	private WebElement nameField;
	@FindBy(id="email")
	private WebElement emailField;
	@FindBy(id="telephone")
	private WebElement telephoneField;
	@FindBy(id="comment")
	private WebElement commentField;
	@FindBy(xpath="html/body/div[1]/div/div[2]/div/div[1]/form/div[2]/button")
	private WebElement submitBtn;
	

	public ContactUsPage(WebDriver driver) {
		super(driver);
		
	}
	
	public ContactUsPage contactUsWith(String name, String email, String phone, String comment) {
		
		try{
			logger.info("The registerWithInformation method has started successfully");
			nameField.sendKeys(name);
			logger.info("name has been entered successfully");
			emailField.sendKeys(email);
			telephoneField.sendKeys(phone);
			commentField.sendKeys(comment);
			logger.info("password has been entered successfully");
			submitBtn.click();
			logger.info("Submit button has been clicked");
		}catch(Throwable t){
			logger.error("The method registerWithInformation has encountered error" +t);
			camera.takeShot("registerWithInformation");
		}

		return PageFactory.initElements(driver, ContactUsPage.class);
	}
	
	public void verifyContactFormSubmission() {
		try{
			logger.info("The verifyContactFormSubmission method has started successfully");
			boolean result = driver.getPageSource().contains(config.getProperty("message"));
			Assert.assertTrue(result, "Test failed because the page does not contain"+ config.getProperty("message"));
			logger.info("message has been verified");
		}catch(Throwable t){
			logger.error("The method verifyContactFormSubmission has encountered error" +t);
			camera.takeShot("verifyContactFormSubmission");
			
		}
		
	}

}
