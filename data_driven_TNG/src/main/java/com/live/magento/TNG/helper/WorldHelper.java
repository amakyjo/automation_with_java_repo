package com.live.magento.TNG.helper;

import org.openqa.selenium.support.PageFactory;

import com.live.magento.TNG.browsers.Browsers;
import com.live.magento.TNG.pages.ContactUsPage;
import com.live.magento.TNG.pages.HomePage;



public class WorldHelper extends Browsers{
	

	private HomePage homePage;
	private ContactUsPage contactUsPage;

	
	public HomePage getHomePage(){
		if(homePage == null){
			homePage = PageFactory.initElements(driver, HomePage.class);
		}
		return homePage;
	}
	
	public ContactUsPage getContactUsPage(){
		if(contactUsPage == null){
			contactUsPage = PageFactory.initElements(driver, ContactUsPage.class);
		}
		return contactUsPage;
	}

}
