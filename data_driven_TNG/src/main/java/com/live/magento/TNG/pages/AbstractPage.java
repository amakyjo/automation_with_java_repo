package com.live.magento.TNG.pages;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.live.magento.TNG.pages.HomePage;
import com.live.magento.TNG.utilities.Screenshot;
import com.live.magento.TNG.utilities.UrlFormatter;




public class AbstractPage {

	protected WebDriver driver;
	protected static Properties config = new Properties();
	protected static InputStream fis;
	protected Screenshot camera;
	protected static Logger logger = LogManager.getLogger("TestLogger");
	
	
	
	public AbstractPage(WebDriver driver){
		this.driver = driver;
		this.camera = new Screenshot(driver);
		
		try {
			fis = new FileInputStream(System.getProperty("user.dir")+ "\\src\\main\\resources\\testData.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			config.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
	public void loadDefaultPage(){
		try{
			logger.info("The loadDefaultPage method is started correctly");
			String formatedUrl = UrlFormatter.formatUrl(config.getProperty("baseUrl"));
			logger.info("The Url has been formatted correctly");
			driver.navigate().to(formatedUrl);
			logger.info("The Web address has been loaded successfully");
		}catch(Throwable t){
			logger.error("The loadDefaultPage Method has encountered error" +t);
			camera.takeShot("loadDefaultPage");
		}
		
	}
	

}
