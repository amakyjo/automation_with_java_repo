package com.live.magento.TNG.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;




public class HomePage extends AbstractPage{

	@FindBy(xpath="html/body/div[1]/div/div[3]/div/div[2]/ul/li[2]/a")
	private WebElement contactUsLink;

	public HomePage(WebDriver driver) {
		super(driver);
	}
	

	public ContactUsPage openContactUsPage(){
		try{
			logger.info("The openContactUsPage method has started successfully");
			contactUsLink.click();
			logger.info("Account has been opened successfully");
		}catch(Throwable t){
			logger.error("The method openContactUsPage has encountered error" +t);
			camera.takeShot("openContactUsPage");
		}
		return PageFactory.initElements(driver, ContactUsPage.class);
	
	}
}
