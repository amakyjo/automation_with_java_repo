package com.live.magento.TNG.test_area;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.live.magento.TNG.excel.utils.GenericExcelReader;
import com.live.magento.TNG.helper.WorldHelper;

public class TestContactUs {
	
	
	private WorldHelper helper;
	private static GenericExcelReader excel = new GenericExcelReader(System.getProperty("user.dir")+"\\src\\test\\resources\\SprintOne\\ContactUsInfo.xlsx");

	@BeforeMethod
	public void setUp() throws Exception{
		helper = new WorldHelper();
		helper.instatiateDriver();
		helper.setUpCleanDriver().loadDefaultPage();
	}
	
	
	@Test(dataProvider = "testData")
	public void testUserRegistrationFunctionality(String name, String email, String phone, String comment){
		helper.getHomePage()
		.openContactUsPage()
		.contactUsWith(name, email, phone, comment)
		.verifyContactFormSubmission();
		
				
	}
	
	
	@DataProvider
	public static Object[][] testData(){
		String sheetName = "sheet1";
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);
		Object[][] data = new Object[rows-1][cols];
		for(int rowNum = 2 ; rowNum <= rows ; rowNum++){ //2
			for(int colNum=0 ; colNum< cols; colNum++){
				data[rowNum-2][colNum]=excel.getCellData(sheetName, colNum, rowNum); //-2
			}
		}
		return data;
	}
	
	@AfterMethod
	public void closeBrowser(){
		helper.destroyDriver();
	}

}
